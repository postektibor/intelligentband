//
//  MessageAction.swift
//  MeshNetworks
//
//  Created by Tibor Postek on 6.9.17.
//  Copyright © 2017 Tibor Postek. All rights reserved.
//

import Foundation
import MultipeerConnectivity

public protocol IMessageAction{
    func executeMessage(message: AnyObject, peerID: MCPeerID)
}
