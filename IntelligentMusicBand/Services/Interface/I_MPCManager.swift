//
//  IMPCManager.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 25.3.18.
//  Copyright © 2018 Tibor Postek. All rights reserved.
//

import Foundation
import MultipeerConnectivity

protocol I_MPCManager {
    func foundPeer()
    func lostPeer()
    func connectedWithPeer(peerID: MCPeerID)
    func disconnectedFromPeer(peerID: MCPeerID, session: MCSession)
    func refreshing()
}
