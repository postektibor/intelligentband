//
//  IRepository.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 25.12.17.
//  Copyright © 2017 Tibor Postek. All rights reserved.
//

import Foundation
import SQLite

protocol IRepository {
    associatedtype T
    static func repositoryInit(db:Connection) -> Void
    static func insert(item: T) -> Int64?
    static func update(item: T) -> Int?
    static func delete(item: T) -> Void
    static func findAll() -> [T]?
    static func findBy(item: T) -> T?
}
