//
//  MidiManager.swift
//  MeshNetworks
//
//  Created by Tibor Postek on 25.9.17.
//  Copyright © 2017 Tibor Postek. All rights reserved.
//

import Foundation
import CoreMIDI

public class MIDI_Manager {
    static private var midiClient: MIDIClientRef = 0;
    static private var outPort:MIDIPortRef = 0;
    static private let userDefaults = UserDefaults.standard
    
    private static func initOfClient(){
        MIDIClientCreate("MidiTestClient" as CFString, nil, nil, &midiClient);
        MIDIOutputPortCreate(midiClient, "MidiTest_OutPort" as CFString, &outPort);
    }
    
    static func sendOpenSongMessage(song: Song){
        initOfClient()
        var packet1:MIDIPacket = MIDIPacket();
        let destinationNames = getDestinationNames()
        let midiControlChannel = self.userDefaults.integer(forKey: "midi_control_channel")-1
        for (index,_) in destinationNames.enumerated()
        {
            let dest:MIDIEndpointRef = MIDIGetDestination(index);
            if song.msb >= 0 {
                packet1.timeStamp = 0;
                packet1.length = 9;
                packet1.data.0 = 0xB0 + UInt8(midiControlChannel) //Control mode on 1th channel + selection //BF je 16. kanal
                packet1.data.1 = 0x63 //Control for NPRN MSB -selection
                packet1.data.2 = UInt8(song.msb) //MSB data set number --toto sa tam posiela
                var packetList:MIDIPacketList = MIDIPacketList(numPackets: 1, packet: packet1);
                MIDISend(outPort, dest, &packetList)
            }
            if song.lsb >= 0 {
                packet1.data.0 = 0xB0 + UInt8(midiControlChannel) //Control mode on 1th channel + selection //BF je 16. kanal
                packet1.data.1 = 0x62 //Control for NPRN LSB -selection
                packet1.data.2 = UInt8(song.lsb) //LSB data set number --toto sa tam posiela
                var packetList = MIDIPacketList(numPackets: 1, packet: packet1);
                MIDISend(outPort, dest, &packetList);
            }
            if song.msb_data >= 0 {
                packet1.data.0 = 0xB0 + UInt8(midiControlChannel) //Control mode on 1th channel + selection //BF je 16. kanal
                packet1.data.1 = 0x06 //Control for Data MSB
                packet1.data.2 = UInt8(song.msb_data) //MSB data --udáva počet stoviek alebo tisicov xx00 v ID piesni
                var packetList = MIDIPacketList(numPackets: 1, packet: packet1);
                MIDISend(outPort, dest, &packetList);
            }
            if song.lsb_data >= 0 {
                packet1.data.0 = 0xB0 + UInt8(midiControlChannel) //Control mode on 1th channel + selection //BF je 16. kanal
                packet1.data.1 = 0x26 //Control for Data LSB
                packet1.data.2 = UInt8(song.lsb_data) //LSB data --udáva desiatky a jednotky 00xx v ID piesni
                var packetList = MIDIPacketList(numPackets: 1, packet: packet1);
                MIDISend(outPort, dest, &packetList);
            }
        }
    }
    
    static func getDisplayName(_ obj: MIDIObjectRef) -> String
    {
        var param: Unmanaged<CFString>?
        var name: String = "Error";
        
        let err: OSStatus = MIDIObjectGetStringProperty(obj, kMIDIPropertyDisplayName, &param)
        if err == OSStatus(noErr)
        {
            name =  param!.takeRetainedValue() as String
        }
        return name;
    }
    
    static func getDestinationNames() -> [String]
    {
        var names:[String] = [String]();
        
        let count: Int = MIDIGetNumberOfDestinations();
        for i in 0 ..< count
        {
            let endpoint:MIDIEndpointRef = MIDIGetDestination(i);
            if (endpoint != 0)
            {
                names.append(getDisplayName(endpoint));
            }
        }
        return names;
    }
}
