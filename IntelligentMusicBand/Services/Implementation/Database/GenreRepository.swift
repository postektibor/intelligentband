//
//  GenreRepository.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 25.12.17.
//  Copyright © 2017 Tibor Postek. All rights reserved.
//

import Foundation
import SQLite

class GenreRepository: IRepository {
    
    static let genre = Table("genre")
    static let genre_id = Expression<Int64>("genre_id")
    static let name = Expression<String>("name")
    
    typealias T = Genre
    static var db: Connection?
    
    
    static func repositoryInit(db:Connection) {
        self.db = db
        do {
            try db.run(genre.create(ifNotExists: true) { table in
                table.column(genre_id, primaryKey: true)
                table.column(name)
            })
        } catch {
            print("Unable to create Genre table")
        }        
        do {
            var insert = genre.insert(or: .replace, genre_id <- 0, name <- "Pop")
            try db.run(insert)
            insert = genre.insert(or: .replace, genre_id <- 1, name <- "Oldies")
            try db.run(insert)
            insert = genre.insert(or: .replace, genre_id <- 2, name <- "Jazz")
            try db.run(insert)
            insert = genre.insert(or: .replace, genre_id <- 3, name <- "Rock")
            try db.run(insert)
            insert = genre.insert(or: .replace, genre_id <- 4, name <- "Polka")
            try db.run(insert)
            insert = genre.insert(or: .replace, genre_id <- 5, name <- "Waltz")
            try db.run(insert)
            insert = genre.insert(or: .replace, genre_id <- 6, name <- "Tango")
            try db.run(insert)
            insert = genre.insert(or: .replace, genre_id <- 7, name <- "Rock&Roll")
            try db.run(insert)
            insert = genre.insert(or: .replace, genre_id <- 8, name <- "Latino")
            try db.run(insert)
            insert = genre.insert(or: .replace, genre_id <- 9, name <- "Country")
            try db.run(insert)
            insert = genre.insert(or: .replace, genre_id <- 10, name <- "Blues")
            try db.run(insert)
            insert = genre.insert(or: .replace, genre_id <- 11, name <- "Reggae")
            try db.run(insert)
            insert = genre.insert(or: .replace, genre_id <- 12, name <- "Hip-Hop")
            try db.run(insert)
        } catch {
            print("Insert failed")
        }
    }
    
    static func insert(item: Genre) -> Int64? {
        do {
            let insert = genre.insert(genre_id <- item.genre_id, name <- item.name!)
            let id = try db?.run(insert)
            return id
        } catch {
            print("Insert failed")
            return nil
        }
    }
    
    static func update(item: Genre) -> Int? {
        do {
            let oldGenre = genre.filter(genre_id == item.genre_id)
            return try db?.run(oldGenre.update(name <- item.name!))
        } catch {
            print("Insert failed")
            return nil
        }
    }
    
    
    static func delete(item: Genre) {
        
    }
    
    static func findAll() -> [Genre]? {
        var genres = [Genre]()
        do {
            for rec in (try db?.prepare(self.genre.order(genre_id)))! {
                genres.append(Genre(
                    genre_id: rec[genre_id],
                    name: rec[name]))
            }
        } catch {
            print("Select failed")
        }
        return genres
    }
    
    static func findBy(item: Genre) -> Genre? {
        do {
            for rec in (try db?.prepare(genre.select(name).filter(genre_id==item.genre_id)))! {
          //      print("name: \(rec[name])")                
                return Genre(genre_id: item.genre_id, name: rec[name])
            }
        } catch {
            print("Select failed")
        }
        return nil
    }
    
}
