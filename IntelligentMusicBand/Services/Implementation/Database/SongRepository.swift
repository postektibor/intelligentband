//
//  SongRepository.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 25.12.17.
//  Copyright © 2017 Tibor Postek. All rights reserved.
//

import Foundation
import SQLite

class SongRepository: IRepository {
    static let song = Table("song")
    static let genre = Table("genre")
    static let url = Expression<String>("url")
    static let music_url = Expression<String?>("music_url")
    static let genre_id = Expression<Int64>("genre_id")
    static let msb = Expression<Int64?>("msb")
    static let msb_data = Expression<Int64?>("msb_data")
    static let lsb = Expression<Int64?>("lsb")
    static let lsb_data = Expression<Int64?>("lsb_data")
    static let count_of_open = Expression<Int64>("count_of_open")
    static let today_open = Expression<Bool>("today_open")
    typealias T = Song
    static var db: Connection?
    
    static func repositoryInit(db:Connection) {
        self.db = db
        do {
            try db.run(song.create(ifNotExists: true) { table in
                table.column(url, primaryKey: true)
                table.column(genre_id)
                table.column(msb)
                table.column(msb_data)
                table.column(lsb)
                table.column(lsb_data)
                table.column(count_of_open)
                table.column(today_open)
                table.foreignKey(genre_id, references: genre, genre_id, delete: .setNull)
            })
            try db.run(song.addColumn(music_url, defaultValue: nil))
        } catch {
            print("Unable to create Song table")
        }
    }
    
    
    static func insert(item: Song) -> Int64? {
        do {
            let insert = song.insert(url <- item.url!, music_url <- item.music_url, genre_id <- item.genre_id, msb <- item.msb, msb_data <- item.msb_data, lsb <- item.lsb, lsb_data <- item.lsb_data, count_of_open <- item.count_of_open, today_open <- item.today_open)
            let id = try db?.run(insert)
            return id
        } catch {
            print("Insert failed")
            return nil
        }
    }
    
    static func update(item: Song) -> Int? {
        do {
            let oldSong = song.filter(url == item.url!)
            return try db?.run(oldSong.update(music_url <- item.music_url, genre_id <- item.genre_id, msb <- item.msb, msb_data <- item.msb_data, lsb <- item.lsb, lsb_data <- item.lsb_data, count_of_open <- item.count_of_open, today_open <- item.today_open))
        } catch {
            print("Update failed")
            return nil
        }
    }
    static func delete(item: Song) {
        
    }
    
    static func maxOfAllOpenedSongs() -> Int64? {
        do {
            return try db?.scalar(song.select(count_of_open.max))
        } catch {
            print("sumOfAllOpenedSongs failed")
        }
        return nil
    }
    
    static func resetAllTodayOpenSongs(){
        if let songs = findAll() {
            for s in songs {
                s.today_open = false
                update(item: s)
            }
        }
    }
    
    static func resetOpenSongsRating(){
        if let songs = findAll() {
            for s in songs {
                s.count_of_open = 0
                update(item: s)
            }
        }
    }
    
    
    static func findAll() -> [Song]? {
        var songs = [Song]()
        do {
            for rec in (try db?.prepare(self.song))! {
                songs.append(Song(
                    url: rec[url],
                    music_url: rec[music_url],
                    msb: rec[msb]!,
                    msb_data: rec[msb_data]!,
                    lsb: rec[lsb]!,
                    lsb_data: rec[lsb_data]!,
                    genre_id: rec[genre_id],
                    count_of_open: rec[count_of_open],
                    today_open: rec[today_open]))
            }
        }catch {
            print("Select failed")
        }
        return songs
    }
    
    static func findBy(item: Song) -> Song? {
        do {
            for rec in (try db?.prepare(song.filter(url==item.url!)))! {
                //print("msb: \(rec[msb]!), msb_data: \(rec[msb_data]!), lsb: \(rec[lsb]!), lsb_data: \(rec[lsb_data]!), genre_id: \(rec[genre_id])")
                return Song(url: item.url!, music_url: rec[music_url], msb: rec[msb]!, msb_data: rec[msb_data]!, lsb: rec[lsb]!, lsb_data: rec[lsb_data]!, genre_id: rec[genre_id], count_of_open: rec[count_of_open], today_open: rec[today_open])
            }
        } catch {
            print("Select failed")
        }
        return nil
    }
}
