//
//  SharedManager.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 11.11.17.
//  Copyright © 2017 Tibor Postek. All rights reserved.
//

import Foundation
import UIKit

class FilesManager {
    //shared initialization
    static let shared = FilesManager ()
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var selectedFiles : [MessageFile]
    var actualOpenFileIndex : Int
    var arrayOfOpenPaths : [URL]
    var delegates = [IDelegate]()    
    
    var editedSongDocument: BrowserFile?
          
    private init(){
        //tu by som mal natahat data z databazy z predoslej relacie
        self.selectedFiles = [MessageFile]()
        self.actualOpenFileIndex = -1
        self.arrayOfOpenPaths = [URL]()
    }
    
    public static func registerDelegate(delegate: IDelegate){
        shared.delegates.append(delegate)
    }
    
    public func refreshDelegates(){
        for del in FilesManager.shared.delegates {
            del.refreshMe()
        }
        FilesManager.appDelegate.mpcManager.reloadTabBarBadgetNumbers()
    }    
}
