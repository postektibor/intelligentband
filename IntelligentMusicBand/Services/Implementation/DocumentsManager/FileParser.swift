import Foundation

class FileParser {
    
    static let sharedInstance = FileParser()
    
    var _includesFileExtensions = [String]()
    var countOfAllOpenedFiles :Int64 = 0
    
    /// Mapped for case insensitivity
    var includesFileExtensions: [String]? {
        get {
            return _includesFileExtensions.map({$0.lowercased()})
        }
        set {
            if let newValue = newValue {
                _includesFileExtensions = newValue
            }
        }
    }
    
    var excludesFilepaths: [URL]?
    
    let fileManager = FileManager.default
    
    
    func documentsURL() -> URL {        
        return fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0] as URL
    }
    
    func filesForDirectory(_ directoryPath: URL, selectedSortingSegmentIndex: Int) -> [BrowserFile]  {
        var files = [BrowserFile]()
        var filePaths = [URL]()
        // Get contents
        do  {
            filePaths = try self.fileManager.contentsOfDirectory(at: directoryPath, includingPropertiesForKeys: [], options: [.skipsHiddenFiles])
        } catch {
            return files
        }
        // Parse
        for filePath in filePaths {
            let file = BrowserFile(filePath: filePath)
            if let excludesFilepaths = excludesFilepaths , excludesFilepaths.contains(file.filePath) {
                continue
            }
            if !file.displayName.isEmpty && file.isDirectory {
                files.append(file)
            }
            if let includesFileExtensions = includesFileExtensions, let fileExtensions = file.fileExtension , includesFileExtensions.contains(fileExtensions) {
                if !file.displayName.isEmpty {
                    file.rating = getRating(file: file)
                    files.append(file)
                }
            }
        }

        // Sort
        switch selectedSortingSegmentIndex {
        case 0:
            files = files.sorted(){0<$0.isDirectoryCompare(file2: $1)}
        case 1:
            files = files.sorted(){$0.displayName.lowercased() < $1.displayName.lowercased()}
        case 2:
            files = files.sorted(){$0.rating > $1.rating}
        default: break
        }
        return files
    }

    
    func getRating(file: BrowserFile) -> Double {
        let documentFile = MessageFile.toMyFile(fbFile: file)
        if !file.isDirectory && SongRepository.findBy(item: Song(url: documentFile.standardizedPath)) != nil{
            let song = SongRepository.findBy(item: Song(url: documentFile.standardizedPath))
            if countOfAllOpenedFiles > 0 {
                return Double((Double((song?.count_of_open)!) / Double(countOfAllOpenedFiles))*5) //because of 5 rating stars
            } else {
                return 0
            }
        }
        return 0
    }

}
