//
//  MPCService.swift
//  MeshNetworks
//
//  Created by Tibor Postek on 8.8.17.
//  Copyright © 2017 Tibor Postek. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class MPCManger: NSObject, MCSessionDelegate, MCNearbyServiceBrowserDelegate, MCNearbyServiceAdvertiserDelegate, IMessageAction{

    
    var myTimer : Timer = Timer()
    let localStorage = UserDefaults.standard
    
    //Multipeer
    var delegate: I_MPCManager?
    var session: MCSession!
    var peer: MCPeerID!
    var browser: MCNearbyServiceBrowser!
    var advertiser: MCNearbyServiceAdvertiser!
    var foundPeers: [MCPeerID]!
    
    var lastConnectedDevices = [MCPeerID]()
    var sendedInvitations = [MCPeerID]()
    var receivedInvitations = [MCPeerID]()
    
    var invitationHandler: ((Bool, MCSession?)->Void)!
    var actionDictionary = [String : IMessageAction]()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private var isPresenter = false
    
    override init()
    {
        super.init()
        UserDefaults.standard.register(defaults: [String : Any]())
        UserDefaults.standard.synchronize()
        //Multipeer
        if UserDefaults.standard.string(forKey: "name_preference") != nil {
            peer = MCPeerID(displayName: UserDefaults.standard.string(forKey: "name_preference")!)
        } else{
            peer = MCPeerID(displayName: UIDevice.current.name)
        }       
        session = MCSession(peer: peer, securityIdentity: nil, encryptionPreference: MCEncryptionPreference.none)
        session.delegate = self
        advertiser = MCNearbyServiceAdvertiser(peer: peer, discoveryInfo: nil, serviceType: "MeshNetworks")
        advertiser.delegate = self
        browser = MCNearbyServiceBrowser(peer: peer, serviceType: "MeshNetworks")
        browser.delegate = self
        initMessageActionsTypes()
        foundPeers = [MCPeerID]()
        
    }
    
    func storeMyInvitedDevices(device: MCPeerID){
        var isUnique = true
        for var peer in self.sendedInvitations {
            if peer.isEqual(device) {
                isUnique=false
            }
        }
        if isUnique {
            self.sendedInvitations.append(device)
        }
    }
    
    
    func isDevicePresenter() -> Bool {
        return isPresenter
    }
    
    func getAllDevices() -> [MCPeerID] {
        var allDevices = [MCPeerID]()
        if !session.connectedPeers.isEmpty {
            allDevices = session.connectedPeers
        }
        for found in foundPeers {
            if !isDeviceConnected(peer: found) && !isMyDevice(device: found) {
                allDevices.append(found)
            }
        }
        return allDevices
    }
    
    func isDeviceConnected(peer: MCPeerID) -> Bool {
        if !session.connectedPeers.isEmpty {
            for device in session.connectedPeers {
                if peer.isEqual(device){
                    return true
                }
            }
        }
        return false
    }
    
    func isMyDevice(device: MCPeerID) -> Bool {
        return peer.isEqual(device)
    }
    
    
    func coutOfUnconnectedDevices() -> Int {
        var count = 0
        for dev in foundPeers {
            if !isMyDevice(device: dev) && !isDeviceConnected(peer: dev){
                count = count + 1
            }
        }
        return count
    }
    
    
    func canInviteDevice(device:MCPeerID) -> Bool {
        for dev in self.receivedInvitations {
            if dev.isEqual(device){
                return  false
            }
        }
        return true
    }
    
    private func removeFromInvitation(device:MCPeerID){
        for (i, dev) in receivedInvitations.enumerated() {
            if dev.isEqual(device){
                self.receivedInvitations.remove(at: i)
            }
        }
    }
    
    func disconnectMeFromSession(){
        appDelegate.mpcManager.session.disconnect()
        removeFromInvitation(device: peer)
    }
    

    
    public func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        NSLog("Finished receiving resource from " + peerID.displayName)
    }
    
    public func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        NSLog("Received invitation from " + peerID.displayName)
        self.invitationHandler = invitationHandler
        if self.canInviteDevice(device: peerID) {
            self.receivedInvitations.append(peerID)
        }
        for myPeer in self.session.connectedPeers {
            if myPeer.isEqual(peerID) {
                self.session.cancelConnectPeer(myPeer)
                self.disconnectMeFromSession()
                self.invitationHandler(true, self.session)
                return
            }
        }
        if self.localStorage.stringArray(forKey: "aceptedPeersArray") != nil {
            let arrayOfAcceptedPeers = self.localStorage.stringArray(forKey: "aceptedPeersArray") ?? [String]()
            for myPeer in arrayOfAcceptedPeers {
                if myPeer == peerID.displayName {
                    self.invitationHandler(true, self.session)
                    return
                }
            }
        }
        perform(#selector(presentAcceptAction(fromPeer:)), with: peerID, afterDelay: 0)
    }
    
    public func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        addNewPeer(peerID: peerID)
        delegate?.foundPeer()
        if !isDeviceConnected(peer: peerID) {
            self.reconnectStoredDevice(device: peerID)
        }
        delegate?.refreshing()
        reloadTabBarBadgetNumbers()
    }
    
    func addNewPeer(peerID: MCPeerID){
        var isUnique = true
        for found in foundPeers {
            if found.isEqual(peerID) || found.isEqual(self.peer) {
                isUnique = false
                break
            }
        }
        if isUnique {
            NSLog("Found peer: " + peerID.displayName)
            foundPeers.append(peerID)
        }
    }
    

    public func clearAllFoundPeers(){
        self.foundPeers.removeAll()
        self.sendedInvitations.removeAll()
        self.receivedInvitations.removeAll()
        delegate?.refreshing()
    }
    
    
    
    private func storeConnectedDevice(device:MCPeerID){
        DispatchQueue.main.async {
            var canStore = false
            for (index, peer) in self.sendedInvitations.enumerated(){
                if peer.isEqual(device) {
                    self.sendedInvitations.remove(at: index)
                    canStore = true
                    break;
                }
            }
        
            if canStore && self.isStoredDevicesUnique(device: device) {
                self.lastConnectedDevices.append(device)
                let messageDictionary: [String: AnyObject] = ["sendMePresenterInfo": self.isPresenter as AnyObject]
                self.appDelegate.mpcManager.sendDataToPeer(dictionaryWithData: messageDictionary, toPeer: device)
            }
        }
    }
    
    func storeAllConnectedDevice(){
        for device in session.connectedPeers {
            storeConnectedDevice(device: device)
        }
    }
    
    private func isStoredDevicesUnique(device:MCPeerID) -> Bool {
        for dev in self.lastConnectedDevices {
            if dev.isEqual(device){
                return  false
            }
        }
        return true
    }
    
    func disconnectDevice(device:MCPeerID) {
        for (i, dev) in lastConnectedDevices.enumerated() {
            if dev.isEqual(device){
                self.lastConnectedDevices.remove(at: i)
            }
        }
        appDelegate.mpcManager.session.cancelConnectPeer(device)
    }
    
    
    func reconnectStoredDevice(device:MCPeerID) {
        for var peer in self.lastConnectedDevices {
            if peer.isEqual(device) {
                self.browser.invitePeer(device, to: self.appDelegate.mpcManager.session, withContext: nil, timeout: 20)
            }
        }
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        for (index, aPeer) in foundPeers.enumerated()
        {
            if aPeer == peerID
            {
                foundPeers.remove(at: index)
                break
            }
        }
        delegate?.lostPeer()
        reloadTabBarBadgetNumbers()
    }
    
    
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error)
    {
        NSLog(error.localizedDescription)
    }
    
    @objc func presentAcceptAction(fromPeer:MCPeerID) {
        let alert = UIAlertController(title: "", message: "\(fromPeer.displayName) wants to join with you.", preferredStyle: UIAlertControllerStyle.alert)
        let acceptAction: UIAlertAction = UIAlertAction(title: "Accept", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.invitationHandler(true, self.session)
           //store accepted peers to local storage
            var arrayOfPeers = [String]()
            if self.localStorage.stringArray(forKey: "aceptedPeersArray") != nil {
                arrayOfPeers = self.localStorage.stringArray(forKey: "aceptedPeersArray") ?? [String]()
                arrayOfPeers.append(fromPeer.displayName)
            }else{
                arrayOfPeers.append(fromPeer.displayName)
            }
            self.localStorage.set(arrayOfPeers, forKey: "aceptedPeersArray")
            self.localStorage.synchronize()
        }
        let declineAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (alertAction) -> Void in
            self.invitationHandler(false, nil)
        }
        alert.addAction(acceptAction)
        alert.addAction(declineAction)
        OperationQueue.main.addOperation { () -> Void in
            UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
        }
    }
   
    
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error)
    {
        NSLog(error.localizedDescription)
    }
    
    
    // MARK: MCSessionDelegate method implementation
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        switch state{
        case MCSessionState.connected:
            NSLog("Connected to session: \(session)\n")
            storeConnectedDevice(device: peerID)
            removeFromInvitation(device: peerID)
            delegate?.connectedWithPeer(peerID: peerID)  
            
        case MCSessionState.connecting:
            NSLog("Connecting to session: \(session)\n")
            delegate?.refreshing()
        case MCSessionState.notConnected:
            NSLog("Session: \(session) is disconected\n")
            session.cancelConnectPeer(peerID)
            delegate?.disconnectedFromPeer(peerID: peerID, session: session)
        }
        reloadTabBarBadgetNumbers()
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        if let dictionaryData: [String : AnyObject] = NSKeyedUnarchiver.unarchiveObject(with: data) as? [String : AnyObject]{
            actionDictionary[(dictionaryData.first?.key)!]?.executeMessage(message: dictionaryData as AnyObject, peerID: peerID)
        }else {
            let alert = UIAlertController(title: "Warning! ", message: "Message from \(peerID.displayName) is incorrect.", preferredStyle: UIAlertControllerStyle.alert)
            let okAction: UIAlertAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (alertAction) -> Void in}
            alert.addAction(okAction)
            OperationQueue.main.addOperation { () -> Void in
                UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
            }
            //hide alert after any seconds
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1){
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
    func reloadTabBarBadgetNumbers(){
        UIApplication.topViewController()?.tabBarController?.updateBadge(indexOfTab: 2, num: coutOfUnconnectedDevices())
        UIApplication.topViewController()?.tabBarController?.updateBadge(indexOfTab: 1, num: FilesManager.shared.selectedFiles.count)
    }
    
    
    
    func setMeAsPresenter(){
        self.isPresenter = true
        for device in session.connectedPeers {
            let messageDictionary: [String: AnyObject] = ["setPresenter": false as AnyObject]
            self.appDelegate.mpcManager.sendDataToPeer(dictionaryWithData: messageDictionary, toPeer: device)
        }
    }
    
    func disableMeAsPresenter(){
        self.isPresenter = false
    }
    
    private func getFileFromDirectory(filePath: String) -> Data? {
        let fileManager = FileManager.default
        return fileManager.contents(atPath: filePath)
    }
    
    
    func executeMessage(message: AnyObject, peerID: MCPeerID) {
        let receivedDataDictionary = message as! Dictionary<String, AnyObject>
        if (receivedDataDictionary["sendMePresenterInfo"] != nil){
            let messageDictionary: [String: AnyObject] = ["isPresenter": self.isPresenter as AnyObject]
            self.appDelegate.mpcManager.sendDataToPeer(dictionaryWithData: messageDictionary, toPeer: peerID)
            
        } else if (receivedDataDictionary["isPresenter"] != nil){
            let isPeerPresenter = receivedDataDictionary["isPresenter"] as! Bool
            //tu viem zistit kto je presenter
        }
        else if (receivedDataDictionary["setPresenter"] != nil){
            self.isPresenter = receivedDataDictionary["setPresenter"] as! Bool
            FilesManager.shared.refreshDelegates()
        }
        else if (receivedDataDictionary["requestForSendingDocumentFile"] != nil){
            let messageFile = receivedDataDictionary["requestForSendingDocumentFile"] as! MessageFile
            if let document = getFileFromDirectory(filePath: messageFile.getFileStdPath()){
                let messageDictionary: [String: AnyObject] = ["sendingNewDocumentFile": MessageFileWithData(file: document, messageFile: messageFile) as AnyObject]
                self.appDelegate.mpcManager.sendDataToPeer(dictionaryWithData: messageDictionary, toPeer: peerID)
            }
            
        }
    }
    
    private func initMessageActionsTypes(){        
        self.actionDictionary.updateValue(self as! IMessageAction, forKey: "sendMePresenterInfo")
        self.actionDictionary.updateValue(self as! IMessageAction, forKey: "isPresenter")
        self.actionDictionary.updateValue(self as! IMessageAction, forKey: "setPresenter")
        
        self.actionDictionary.updateValue(self as! IMessageAction, forKey: "requestForSendingDocumentFile")
       
        self.actionDictionary.updateValue((UIApplication.shared.delegate as! AppDelegate).getViewController(identifierVC: "ShowFileViewController")  as! IMessageAction, forKey: "switchAnotherFile")
        self.actionDictionary.updateValue((UIApplication.shared.delegate as! AppDelegate).getViewController(identifierVC: "ShowFileViewController")  as! IMessageAction, forKey: "setNewZoomAndScroll")
        self.actionDictionary.updateValue((UIApplication.shared.delegate as! AppDelegate).getViewController(identifierVC: "ShowFileViewController")  as! IMessageAction, forKey: "sendingNewDocumentFile")
       
        self.actionDictionary.updateValue((UIApplication.shared.delegate as! AppDelegate).getViewController(identifierVC: "FileBrowserViewController")  as! IMessageAction, forKey: "deleteSelectedFileFromFileBrowserVC")
        self.actionDictionary.updateValue((UIApplication.shared.delegate as! AppDelegate).getViewController(identifierVC: "FileBrowserViewController")  as! IMessageAction, forKey: "addNewSelectedFileFileBrowserVC")
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) { }
    
    func session(session: MCSession!, didFinishReceivingResourceWithName resourceName: String!, fromPeer peerID: MCPeerID!, atURL localURL: NSURL!, withError error: NSError!) { }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) { }
    
    // MARK: Custom method implementation
    
    func sendDataToPeer(dictionaryWithData dictionary: Dictionary<String, AnyObject>, toPeer targetPeer: MCPeerID) -> Bool {
        let dataToSend = NSKeyedArchiver.archivedData(withRootObject: dictionary)
        let peersArray = NSArray(object: targetPeer)
        do {
            try session.send(dataToSend, toPeers: peersArray as! [MCPeerID], with: MCSessionSendDataMode.reliable)
        }
        catch _ {
            return false
        }
        return true
    }
        
    func sendDataToAllConnectedPeers(dictionaryWithData dictionary: Dictionary<String, AnyObject>) -> Bool {
        let dataToSend = NSKeyedArchiver.archivedData(withRootObject: dictionary)
        do {
            try session.send(dataToSend, toPeers: self.session.connectedPeers, with: MCSessionSendDataMode.reliable)
        }
        catch _ {
            return false
        }
        return true
    }
    
    
    func sendImageToAllConnectedPeers(img: UIImage) {
        if let imageData = UIImagePNGRepresentation(img) {
            do {
                try session.send(imageData, toPeers: self.session.connectedPeers, with: MCSessionSendDataMode.reliable)
            }
            catch let error {
                NSLog(error as! String)
            }
        }
    }    
}
