//
//  DropBoxViewController.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 13.11.17.
//  Copyright © 2017 Tibor Postek. All rights reserved.
//

import UIKit
import SwiftyDropbox

class DropboxSynchronizationViewController: UIViewController {
    
    @IBOutlet weak var progress: UIProgressView!
    @IBOutlet weak var fileName: UILabel!
    @IBOutlet weak var activityWheel: UIActivityIndicatorView!
    @IBOutlet weak var syncButton: UIBarButtonItem!
    @IBOutlet weak var topNavigationBar: UINavigationItem!
    @IBOutlet weak var logo: UIImageView!
    
    let group = DispatchGroup()
    private let downloadingQueue = DispatchQueue(label: "DownloadingQueue")
    private let countingFilesQueue = DispatchQueue(label: "CountingFilesQueue")
    private let semaphoreDownloading = DispatchSemaphore(value: 5) //maximal parralel download requests
    private let semaphoreCountingFiles = DispatchSemaphore(value: 0)
    private let documentsPath = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
    var dirDictionary = [String : String]()
    private var synchronizationIsActive = false
    
    private var countOfListFiles = 0
    private var countOfProcessedFiles = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        verifyUser()
        logo.tintColor = UIColor(red: 255/255, green: 45/255, blue: 85/255, alpha: 1)
    }
    
    private func verifyUser(){
        // Verify user is logged into Dropbox
        if let client = DropboxClientsManager.authorizedClient {
            client.users.getCurrentAccount().response { response, error in
                if let account = response {
                    self.topNavigationBar.title = "Hello \(account.name.givenName)!"
                } else {
                    print(error!)
                }
            }
        } else {
            loginToDropbox()
        }
    }
    
    @IBAction func logout(_ sender: Any) {
        DropboxClientsManager.unlinkClients()
        if DropboxClientsManager.authorizedClient == nil {
            loginToDropbox()
        }
    }
    
    public func listDropboxFolder(){
        if let client = DropboxClientsManager.authorizedClient {
            self.dirDictionary.removeAll()
            // List folder with recursion of subfolders
            client.files.listFolder(path: "", recursive: true, includeMediaInfo: true, includeDeleted: true, includeHasExplicitSharedMembers: false, includeMountedFolders: true, limit: 1000, sharedLink: nil).response { response, error in
                if let result = response {
                    for entry in result.entries {
                        if ((entry as? Files.FolderMetadata) != nil) {
                            self.parseFolder(folder: entry as! Files.FolderMetadata)
                        } else if ((entry as? Files.FileMetadata) != nil) {
                            self.parseFileAndCallDownload(file: entry as! Files.FileMetadata)
                        } else if ((entry as? Files.DeletedMetadata) != nil) {
                            self.parseDeletedMetadata(deletedMetadata: entry as! Files.DeletedMetadata)
                        }
                    }
                    if result.hasMore {
                        self.continueListFolder(cursor: result.cursor)
                    }
                    
                    if  self.countOfListFiles == 0 {
                        self.enableSyncActivityUI(value: false)
                        self.synchronizationIsActive = false
                    }
                } else {
                    print(error!)
                }
            }
        } else {
            loginToDropbox()
        }
    }
    
    private func continueListFolder(cursor : String){
        if let client = DropboxClientsManager.authorizedClient {
            client.files.listFolderContinue(cursor: cursor).response { response, error in
                if let result = response {
                    for entry in result.entries {
                        if ((entry as? Files.FolderMetadata) != nil) {
                            self.parseFolder(folder: entry as! Files.FolderMetadata)
                        } else if ((entry as? Files.FileMetadata) != nil) {
                            self.parseFileAndCallDownload(file: entry as! Files.FileMetadata)
                        } else if ((entry as? Files.DeletedMetadata) != nil) {
                            self.parseDeletedMetadata(deletedMetadata: entry as! Files.DeletedMetadata)
                        }
                    }
                    if result.hasMore {
                        self.continueListFolder(cursor: result.cursor)
                    }
                } else {
                    print(error!)
                }
            }
        } else {
            loginToDropbox()
        }
    }
    
    public func counterOfDropboxFolders(){
        if let client = DropboxClientsManager.authorizedClient {
            self.enableSyncActivityUI(value: true)
            self.countOfProcessedFiles = 0
            self.countOfListFiles = 0
            client.files.listFolder(path: "", recursive: true, includeMediaInfo: true, includeDeleted: true, includeHasExplicitSharedMembers: false, includeMountedFolders: true, limit: 2000, sharedLink: nil).response { response, error in
                if let result = response {
                    self.countingEntriesOfDropboxSupportedFunctions(entries: result.entries)
                    if result.hasMore {
                        self.continueCounterOfDropboxFolders(cursor: result.cursor)
                    }else {
                        self.semaphoreCountingFiles.signal()
                    }
                } else {
                    print(error!)
                }
            }
        } else {
            loginToDropbox()
        }
    }
    private func continueCounterOfDropboxFolders(cursor : String){
        if let client = DropboxClientsManager.authorizedClient {
            client.files.listFolderContinue(cursor: cursor).response { response, error in
                if let result = response {
                    self.countingEntriesOfDropboxSupportedFunctions(entries: result.entries)
                    if result.hasMore {
                        self.continueCounterOfDropboxFolders(cursor: result.cursor)
                    } else {
                        self.semaphoreCountingFiles.signal()
                    }
                } else {
                    print(error!)
                }
            }
        } else {
            loginToDropbox()
        }
    }
    
    
    private func countingEntriesOfDropboxSupportedFunctions(entries :Array<Files.Metadata>){
        for entry in entries {
            if ((entry as? Files.FolderMetadata) != nil) || ((entry as? Files.FileMetadata) != nil) || ((entry as? Files.DeletedMetadata) != nil) {
                self.countOfListFiles = self.countOfListFiles + 1
            }
        }
    }
    
    public func enableSyncActivityUI(value: Bool){
        DispatchQueue.main.async{
            if value {
                self.activityWheel.startAnimating()
                self.activityWheel.isHidden = false
                self.progress.isHidden = false
                self.fileName.isHidden = false
                self.fileName.text = "Syncing ..."
                self.syncButton.isEnabled = false
            } else {
                self.activityWheel.stopAnimating()
                self.activityWheel.isHidden = true
                self.progress.isHidden = true
                self.fileName.isHidden = true
                self.fileName.text = " "
                self.syncButton.isEnabled = true
            }
        }
    }
    

    private func parseFileAndCallDownload(file: Files.FileMetadata){
        self.downloadFile(file: file)
    }
    
    private func parseFolder(folder: Files.FolderMetadata){
        DispatchQueue.main.async{
            //self.fileName.text = "Creating Directory..."
            self.fileName.text = "Syncing (\(self.countOfProcessedFiles) of \(self.countOfListFiles))."
        }        
        if let folderPath = getPathOfNewFolderInDocumentsCaseInsensitive(folder: folder) {
            registerNewFolder(folder: folderPath.path)
        } else {
            registerNewFolder(folder: (documentsPath.appendingPathComponent("Dropbox\(folder.pathDisplay!)")?.path)!)
        }
        self.countOfProcessedFiles = self.countOfProcessedFiles + 1
        self.checkStatusOfSynchronization()
    }
    
    func registerNewFolder(folder: String){
        self.dirDictionary.updateValue(folder, forKey: folder.lowercased())
        if !existsInFolder(pathString: folder) {
            createFolder(folderPath: folder)
        }
    }
    
    private func parseDeletedMetadata(deletedMetadata: Files.DeletedMetadata){
        DispatchQueue.main.async{
            //self.fileName.text = "Removing..."
            self.fileName.text = "Syncing (\(self.countOfProcessedFiles) of \(self.countOfListFiles))."
        }
        let existedFileURL = self.getPathOfExistedFileInDocumentsCaseInsensitive(filePath: deletedMetadata.pathDisplay, fileName: deletedMetadata.name)
        if let existedFileURL2 = existedFileURL{
            removeFile(fileURL: existedFileURL2)
        }
        self.countOfProcessedFiles = self.countOfProcessedFiles + 1
        self.checkStatusOfSynchronization()
    }
    
    private func downloadFile(file: Files.FileMetadata) {
        //file.serverModified
        self.downloadingQueue.async(group: self.group) {
            self.semaphoreDownloading.wait()
            DispatchQueue.main.async{
                //self.fileName.text = "\(file.name)"
                self.fileName.text = "Syncing (\(self.countOfProcessedFiles) of \(self.countOfListFiles))."
            }

            if let filePath = file.pathDisplay {
                let pathOfExistedFileInDirectory = self.getPathOfExistedFileInDocumentsCaseInsensitive(filePath: filePath, fileName: file.name)
                //if file exists in documents directory
                if pathOfExistedFileInDirectory != nil {
                    
                    if let storedFileDate = self.getDateOfLastModification(fileURL: pathOfExistedFileInDirectory!) {
                        //tu budem porovnavat podla datumov a potom sa rozhodnem ci ho stiahnem alebo nie
                        if storedFileDate < file.serverModified {
                            let pathOfNewFileInDirectory = self.getPathOfNewFileInDocumentsCaseInsensitive(file: file)
                            self.downloadFile(fromServer: file.pathDisplay!, toDirectory: pathOfNewFileInDirectory)
                            return
                        }
                    } else {
                        let pathOfNewFileInDirectory = self.getPathOfNewFileInDocumentsCaseInsensitive(file: file)
                        self.downloadFile(fromServer: file.pathDisplay!, toDirectory: pathOfNewFileInDirectory)
                    }
                    
                    self.countOfProcessedFiles = self.countOfProcessedFiles + 1
                    self.checkStatusOfSynchronization()
                    self.semaphoreDownloading.signal()
                    return
                } else {
                    let pathOfNewFileInDirectory = self.getPathOfNewFileInDocumentsCaseInsensitive(file: file)
                    self.downloadFile(fromServer: file.pathDisplay!, toDirectory: pathOfNewFileInDirectory)
                }
            }
        }
    }
    
    
    private func downloadFile(fromServer: String, toDirectory: URL){
        let destination: (URL, HTTPURLResponse) -> URL = { temporaryURL, response in return toDirectory}
        let client = DropboxClientsManager.authorizedClient!
        client.files.download(path: fromServer, overwrite: true, destination: destination)
            .response { response, error in
                if let response = response {
                    self.countOfProcessedFiles = self.countOfProcessedFiles + 1
                    self.semaphoreDownloading.signal()
                    self.checkStatusOfSynchronization()
                } else  if let error = error {
                    print(error)
                }
        }
    }
    
    
    private func getDateOfLastModification(fileURL: URL) -> Date? {
        do{
            let attr = try FileManager.default.attributesOfItem(atPath: fileURL.path)
            if let fileSize = attr[FileAttributeKey.modificationDate] {
                let pom = fileSize as! Date
                
                return fileSize as? Date
            }
        }
        catch {
            print(error)
        }
        return nil;
    }
    
    private func checkStatusOfSynchronization(){
        DispatchQueue.main.async{
            self.progress.setProgress(Float(self.countOfProcessedFiles)/Float(self.countOfListFiles), animated: true)
            if self.countOfProcessedFiles == self.countOfListFiles {
                self.synchronizationIsActive = false
            }}
        if self.countOfProcessedFiles == self.countOfListFiles {
            self.enableSyncActivityUI(value: false)
        }
    }

    private func getPathOfExistedFileInDocumentsCaseInsensitive(filePath: String?, fileName: String) -> URL? {
        if let filePath2 = filePath {
            let newFilePath = "Dropbox\(filePath2)"
            var destURL = self.documentsPath.appendingPathComponent(newFilePath)
            let destDirectoryURL = destURL?.deletingLastPathComponent()
            if let dir = self.dirDictionary[(destDirectoryURL?.path)!.lowercased().removingPercentEncoding!] {
                let newUrl = URL(fileURLWithPath: dir).appendingPathComponent(fileName)
                destURL = newUrl
            }            
            if FileManager.default.fileExists(atPath: (destURL?.path)!) {
                return (destURL)!
            } else {
                return nil
            }
        }
        return nil
    }
    
    private func getPathOfNewFileInDocumentsCaseInsensitive(file: Files.FileMetadata) -> URL {
        var newFilePath = "Dropbox"
        let destURL = self.documentsPath.appendingPathComponent(newFilePath)
        if let filePath = file.pathDisplay {
            newFilePath = "Dropbox\(filePath)"
            var destURL = self.documentsPath.appendingPathComponent(newFilePath)
            let destDirectoryURL = destURL?.deletingLastPathComponent()
            if let dir = self.dirDictionary[(destDirectoryURL?.path)!.lowercased().removingPercentEncoding!] {
                let newUrl = URL(fileURLWithPath: dir).appendingPathComponent(file.name)
                destURL = newUrl
            }
            return destURL!
        }
        return destURL!
    }
    
    private func getPathOfNewFolderInDocumentsCaseInsensitive(folder: Files.FolderMetadata) -> URL? {
        if let filePath = folder.pathDisplay {
            let newFilePath = "Dropbox\(filePath)"
            var destURL = self.documentsPath.appendingPathComponent(newFilePath)
            let destDirectoryURL = destURL?.deletingLastPathComponent()
            if let dir = self.dirDictionary[(destDirectoryURL?.path)!.lowercased().removingPercentEncoding!] {
                let newUrl = URL(fileURLWithPath: dir).appendingPathComponent(folder.name)
                destURL = newUrl
            }
            return destURL!
        }
        return nil
    }
    
    
    
    
    
    
    private func createFolderInDocuments(folderPath: String){
        let path = documentsPath.appendingPathComponent(folderPath)
        do {
            try FileManager.default.createDirectory(atPath: path!.path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            NSLog("Unable to create directory \(error.debugDescription)")
        }
    }
    
    private func createFolder(folderPath: String){
        do {
            try FileManager.default.createDirectory(atPath: folderPath, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            NSLog("Unable to create directory \(error.debugDescription)")
        }
    }

    private func removeFile(fileURL: URL){
        do {
            try FileManager.default.removeItem(at: fileURL)
        } catch let error as NSError {
            NSLog("Unable to remove file \(error.debugDescription)")
        }
    }

    
    private func existsInDocumentsFolder(pathString: String) -> Bool {
        let filePath = documentsPath.appendingPathComponent(pathString)
        return FileManager.default.fileExists(atPath: (filePath?.path)!)
    }
   
    private func existsInFolder(pathString: String) -> Bool {
        return FileManager.default.fileExists(atPath: pathString)
    }
    
    func loginToDropbox(){
        DropboxClientsManager.authorizeFromController(UIApplication.shared,
                                                      controller: self,
                                                      openURL: { (url: URL) -> Void in
                                                        UIApplication.shared.openURL(url)
        })
    }
    
    @IBAction func synchronizeDataFromDropbox(_ sender: Any) {
        if DropboxClientsManager.authorizedClient != nil {
            if !synchronizationIsActive {
                self.synchronizationIsActive = true
                self.countingFilesQueue.async(group: self.group) {
                    self.counterOfDropboxFolders()
                    self.semaphoreCountingFiles.wait()
                    self.listDropboxFolder()
                }
            }
            // Do any additional setup after loading the view.
        } else {
            loginToDropbox()
        }
    }
    
    
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
