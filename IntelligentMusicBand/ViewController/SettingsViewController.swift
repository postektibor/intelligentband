//
//  SettingsViewController.swift
//  MeshNetworks
//
//  Created by Tibor Postek on 15.8.17.
//  Copyright © 2017 Tibor Postek. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    let userDefaults = UserDefaults.standard
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var controllChannel: UILabel!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var visibility: UISwitch!
    @IBOutlet weak var controllChannelStepper: UIStepper!
    @IBOutlet weak var zoomScrollGestures: UISwitch!
    @IBOutlet weak var resetView: UIView!
    @IBOutlet weak var actionActivity: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        UserDefaults.standard.register(defaults: [String : Any]())
        loadSettings()
        setResetView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)        
    }
    
    
    @IBAction func resetRating(_ sender: Any) {
        self.actionActivity.isHidden = false
        self.resetView.isUserInteractionEnabled = false
        DispatchQueue.global().async {
            SongRepository.resetOpenSongsRating()
            DispatchQueue.main.async {
                self.actionActivity.isHidden = true
                self.hideActionView()
            }
        }
    }
    
    @IBAction func resetTodayOpenFiles(_ sender: Any) {
        self.actionActivity.isHidden = false
        self.resetView.isUserInteractionEnabled = false
        DispatchQueue.global().async {
            SongRepository.resetAllTodayOpenSongs()
            DispatchQueue.main.async {
                self.actionActivity.isHidden = true
                self.hideActionView()
            }
        }
    }
    
    
    @IBAction func actionButton(_ sender: Any) {
        if self.resetView.isHidden {
            showActionView()
        } else {
            hideActionView()
        }
    }
    
    func hideActionView(){
        self.resetView.isHidden = true
    }
    
    func showActionView(){
        self.resetView.isHidden = false
        self.resetView.isUserInteractionEnabled = true
    }
    
    private func setResetView(){
        self.resetView.layer.shadowColor = UIColor.black.cgColor
        self.resetView.layer.shadowOffset = CGSize.zero
        self.resetView.layer.shadowRadius = 5.0
        self.resetView.layer.cornerRadius = 5.0
        self.resetView.layer.shadowOpacity = 0.5
    }
    
    
    
    
    
    
    @IBAction func resetSettings(_ sender: Any) {
        let alertController = UIAlertController(title: "Are you sure?", message: "Do you want reset all stored settings?", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
            for key in UserDefaults.standard.dictionaryRepresentation().keys {
                UserDefaults.standard.removeObject(forKey: key.description)
            }
        })
        let noAction = UIAlertAction(title: "No", style: UIAlertActionStyle.cancel)
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func saveSettings(_ sender: Any) {
        appDelegate.mpcManager.advertiser.stopAdvertisingPeer()
        self.userDefaults.set(self.name.text, forKey: "name_preference")
        self.userDefaults.set(self.visibility.isOn, forKey: "visibility_switch")
        self.userDefaults.set(self.zoomScrollGestures.isOn, forKey: "zoom_scroll_gestures")
        if self.visibility.isOn {
            appDelegate.mpcManager.advertiser.startAdvertisingPeer()
        }
        self.userDefaults.set(Int.init(self.controllChannel.text!), forKey: "midi_control_channel")
        let alertController = UIAlertController(title: "Success!", message: "All settings was stored!", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            self.view.endEditing(true)
        })
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func changeControllChannel(_ sender: Any) {
        self.controllChannel.text = "\(Int.init(self.controllChannelStepper.value))"
    }
    
    
    func loadSettings(){
        self.name.text = self.userDefaults.string(forKey: "name_preference")
        self.visibility.setOn(userDefaults.bool(forKey: "visibility_switch"), animated: true)
        self.zoomScrollGestures.setOn(userDefaults.bool(forKey: "zoom_scroll_gestures"), animated: true)
        //midi_controll_channel
        self.controllChannel.text = "\(self.userDefaults.integer(forKey: "midi_control_channel"))"
        self.controllChannelStepper.value = Double.init(self.userDefaults.integer(forKey: "midi_control_channel"))
    }
    
    
}
