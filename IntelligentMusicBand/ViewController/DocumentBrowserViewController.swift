//
//  FileListViewController.swift
//  FileBrowser
//
//  Created by Roy Marmelstein on 12/02/2016.
//  Copyright © 2016 Roy Marmelstein. All rights reserved.
//

import Foundation
import UIKit
import MultipeerConnectivity

class DocumentBrowserViewController: UIViewController, IMessageAction {
    
    internal let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    // TableView
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topNavigationBar: UINavigationBar!
    @IBOutlet weak var upDirectoryButton: UIBarButtonItem!
    @IBOutlet weak var sortingSegment: UISegmentedControl!
    @IBOutlet weak var playlistView: UIView!
    @IBOutlet weak var actionButton: UIBarButtonItem!
    
    private let refreshControl = UIRefreshControl()
    
    @IBOutlet weak var loadingWheel: UIActivityIndicatorView!
    
    
    /// Data
    var files = [BrowserFile]()
    var initialPath: URL? = FileParser.sharedInstance.documentsURL()
    let parser = FileParser.sharedInstance
    var allowEditing: Bool = false    
    
    // Search controller
    var filteredFiles = [BrowserFile]()
    var isSearching = false
    
    let userDefaults = UserDefaults.standard
    var countOfAllOpenedFiles: Int64?
    
    let searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.searchBarStyle = .default
        searchController.searchBar.backgroundColor = UIColor.white
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search here..."
        searchController.searchBar.returnKeyType = UIReturnKeyType.done
        return searchController
    }()
    
    
    func prepareData() {
        self.parser.includesFileExtensions = ["pdf"]
        if self.appDelegate.sharedManager.arrayOfOpenPaths.isEmpty {
            if let initialPath = self.self.initialPath {
                self.files = self.self.parser.filesForDirectory(initialPath, selectedSortingSegmentIndex: self.sortingSegment.selectedSegmentIndex)
                self.self.indexFiles()
            }
        } else{
            self.initialPath = self.appDelegate.sharedManager.arrayOfOpenPaths.last
            self.files = self.parser.filesForDirectory(self.initialPath!, selectedSortingSegmentIndex: self.self.sortingSegment.selectedSegmentIndex)
            self.indexFiles()
        }
        self.countOfAllOpenedFiles = SongRepository.maxOfAllOpenedSongs()
        if self.countOfAllOpenedFiles != nil {
            self.self.parser.countOfAllOpenedFiles = self.countOfAllOpenedFiles!
        } else {
            self.parser.countOfAllOpenedFiles = 0
        }
    }
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        prepareData()
        // Set search bar
        tableView.tableHeaderView = searchController.searchBar
        indexFiles()
        self.topNavigationBar.topItem?.title = initialPath?.lastPathComponent
        tableView.delegate = self
        tableView.dataSource = self
        searchController.searchBar.delegate = self
        setRefreshPanel()
        tableView.reloadData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotifications(notification:)), name: NSNotification.Name(rawValue: "receivedMPCFileBrowser"), object: nil)
        setPlaylistView()
    }
    
    private func setRefreshPanel() {
        refreshControl.backgroundColor = UIColor.groupTableViewBackground
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshTableData(_:)), for: .valueChanged)
    }
    
    private func setPlaylistView(){
        self.playlistView.layer.shadowColor = UIColor.black.cgColor
        self.playlistView.layer.shadowOffset = CGSize.zero
        self.playlistView.layer.shadowRadius = 5.0
        self.playlistView.layer.cornerRadius = 5.0
        self.playlistView.layer.shadowOpacity = 0.5
    }
    
    
    @objc private func refreshTableData(_ sender: Any) {
        self.prepareData()
        OperationQueue.main.addOperation {
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    
    
    @IBAction func changeMethodOfSorting(_ sender: Any) {
        self.loadingWheel.isHidden = false
        DispatchQueue.global().async {
            self.prepareData()
            DispatchQueue.main.async {            
                self.tableView.reloadData()
                self.loadingWheel.isHidden = true
            }
        }
    }

    func executeMessage(message: AnyObject, peerID: MCPeerID) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "receivedMPCFileBrowser"), object: MessageWithSender(message: message, sender: peerID) as AnyObject)
    }
    
    @objc func handleNotifications(notification: NSNotification){       
        let myMessageSender = notification.object as! MessageWithSender
        let receivedDataDictionary = myMessageSender.message as! Dictionary<String, AnyObject>
        if (receivedDataDictionary["deleteSelectedFileFromFileBrowserVC"] != nil) {
            let myFile = receivedDataDictionary["deleteSelectedFileFromFileBrowserVC"] as! MessageFile
            if appDelegate.sharedManager.selectedFiles.isEmpty{
                appDelegate.sharedManager.actualOpenFileIndex = -1
            } else {
                if (appDelegate.sharedManager.selectedFiles.count > myFile.indexInSelectedFiles && myFile.indexInSelectedFiles > -1){
                    if isEqualFiles(file1: myFile, file2: appDelegate.sharedManager.selectedFiles[myFile.indexInSelectedFiles]) {
                        appDelegate.sharedManager.selectedFiles.remove(at: myFile.indexInSelectedFiles)
                        setIndexOfSelectedFile(deletedAt: myFile.indexInSelectedFiles)
                    } else {
                        findAndRemoveSelectedFile(mFile: myFile)
                    }
                } else {
                    findAndRemoveSelectedFile(mFile: myFile)
                }
            }
        }
        if (receivedDataDictionary["addNewSelectedFileFileBrowserVC"] != nil) {
            let myFile = receivedDataDictionary["addNewSelectedFileFileBrowserVC"] as! MessageFile
            if !isDuplicateOfSelectedFile(file: myFile) {
                if !self.existsFileInDirectory(filePath: myFile.getFileStdPath()) {
                    //request sender for document file
                    var messageDictionary: [String: AnyObject] = ["requestForSendingDocumentFile": myFile as AnyObject]
                    self.appDelegate.mpcManager.sendDataToPeer(dictionaryWithData: messageDictionary, toPeer: myMessageSender.sender)
                }
                appDelegate.sharedManager.selectedFiles.append(myFile)
            }
        }
        let childView = self.childViewControllers.last as! PlaylistViewController
        childView.reloadTable()
        FilesManager.shared.refreshDelegates()
    }

    private func existsFileInDirectory(filePath: String) -> Bool {
        let fileManager = FileManager.default
        return fileManager.fileExists(atPath: filePath)
    }    
    
    private func isDuplicateOfSelectedFile(file: MessageFile) -> Bool {
        for fil in appDelegate.sharedManager.selectedFiles {
            if isEqualFiles(file1: fil, file2: file) {
                return true
            }
        }
        return false
    }
    
    private func isEqualFiles(file1: MessageFile, file2: MessageFile) -> Bool {
        return file1.standardizedPath == file2.standardizedPath
    }
    
    private func findAndRemoveSelectedFile(mFile: MessageFile) {
         for (i, file) in appDelegate.sharedManager.selectedFiles.enumerated() {
            if isEqualFiles(file1: mFile, file2: file) && appDelegate.sharedManager.selectedFiles.count > i{
                appDelegate.sharedManager.selectedFiles.remove(at: i)
                setIndexOfSelectedFile(deletedAt: i)
            }
        }
    }
    
    private func setIndexOfSelectedFile(deletedAt: Int){
        if !appDelegate.sharedManager.selectedFiles.isEmpty {
            if appDelegate.sharedManager.selectedFiles.count >= deletedAt {
                appDelegate.sharedManager.actualOpenFileIndex = deletedAt
            } else {
                appDelegate.sharedManager.actualOpenFileIndex = appDelegate.sharedManager.selectedFiles.count-1
            }
        } else {
            appDelegate.sharedManager.actualOpenFileIndex = -1
        }
    }
    
    
    
    @IBAction func actionButton(_ sender: Any) {
        if self.playlistView.isHidden {
           showActionView()
        } else {
            hideActionView()
        }
    }
    
    func hideActionView(){
        self.playlistView.isHidden = true
    }
    
    func showActionView(){
        self.playlistView.isHidden = false
       self.playlistView.isUserInteractionEnabled = true
        let childView = self.childViewControllers.last as! PlaylistViewController
        childView.reloadTable()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            let offset = CGPoint.init(x: 0, y: self.searchController.searchBar.bounds.height)
            self.tableView.setContentOffset(offset, animated: false)            
        }       
        
        // Make sure navigation bar is visible
        self.navigationController?.isNavigationBarHidden = false
        let childView = self.childViewControllers.last as! PlaylistViewController
        childView.reloadTable()        
    }
    
    @objc func dismiss(button: UIBarButtonItem = UIBarButtonItem()) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Data
    
    func indexFiles() {
        checkUpDirectoryButtonVisibility()
    }
    
    func fileForIndexPath(_ indexPath: IndexPath) -> BrowserFile {
        var file = BrowserFile(filePath: URL(fileURLWithPath: ""))
        if isSearching {
            if filteredFiles.count > (indexPath as NSIndexPath).row {
                file = filteredFiles[(indexPath as NSIndexPath).row]
            }
        }
        else {
            if files.count > (indexPath as NSIndexPath).row {
                file = files[(indexPath as NSIndexPath).row]
            }
        }
        return file
    }
    
    
    
    @IBAction func upDirectory(_ sender: Any) {
        self.loadingWheel.isHidden = false
        DispatchQueue.global().async {
            self.initialPath = self.appDelegate.sharedManager.arrayOfOpenPaths.popLast()?.deletingLastPathComponent()            
            self.prepareData()
            DispatchQueue.main.async {
                self.checkUpDirectoryButtonVisibility()
                self.topNavigationBar.topItem?.title = self.initialPath?.lastPathComponent
                self.tableView.reloadData()
                self.loadingWheel.isHidden = true
            }
        }
    }
    
    private func checkUpDirectoryButtonVisibility(){
        self.upDirectoryButton.isEnabled = true
        if appDelegate.sharedManager.arrayOfOpenPaths.count == 0 {
            self.upDirectoryButton.isEnabled = false
        }
    }
    
}

