//
//  ShowFileViewController.swift
//  MeshNetworks
//
//  Created by Tibor Postek on 5.9.17.
//  Copyright © 2017 Tibor Postek. All rights reserved.
//

import UIKit
import CoreData
import MultipeerConnectivity
import AVFoundation

class DocumentView_ViewController: UIViewController, IMessageAction, IDelegate{    
    

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var pickerSelectFilesView: UIPickerView!
    @IBOutlet weak var topNavigationBar: UINavigationBar!
    //    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var viewUnderDevicesAndSongs: UIView!
    @IBOutlet weak var loadingWheel: UIActivityIndicatorView!
    @IBOutlet weak var removeSong: UIButton!
    @IBOutlet weak var nextSong: UIButton!
    @IBOutlet weak var previous: UIButton!
    let userDefaults = UserDefaults.standard
    
    var timer: Timer?
    
    var isRefreshingSong = true
    var isScrolling = false
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pickerSelectFilesView.dataSource = self
        self.pickerSelectFilesView.delegate = self
        if webView != nil {
            self.webView.scrollView.delegate = self
            webView.scrollView.minimumZoomScale = 0.5
            webView.scrollView.maximumZoomScale = 10.0
            webView.scalesPageToFit = true
        }
        validateOpeningFile(isRefreshing: false)
        hideElementsUI()

        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotifications(notification:)), name: NSNotification.Name(rawValue: "receivedMPCShowFile"), object: nil)
        FilesManager.registerDelegate(delegate: self)
        setViewUnderDevicesAndSongs()
    }
    
    private func setViewUnderDevicesAndSongs(){
        self.viewUnderDevicesAndSongs.layer.shadowColor = UIColor.black.cgColor
        self.viewUnderDevicesAndSongs.layer.shadowOffset = CGSize.zero
        self.viewUnderDevicesAndSongs.layer.shadowRadius = 5.0
        self.viewUnderDevicesAndSongs.layer.cornerRadius = 5.0
        self.viewUnderDevicesAndSongs.layer.shadowOpacity = 0.5
    }
    
    private func validateOpeningFile(isRefreshing: Bool){
        DispatchQueue.main.async {
            self.pickerSelectFilesView.reloadAllComponents()
        }
        if appDelegate.sharedManager.actualOpenFileIndex == -1{
            appDelegate.sharedManager.actualOpenFileIndex = 0
            openFile(index: appDelegate.sharedManager.actualOpenFileIndex, isRefreshing: isRefreshing)
        } else {
            openFile(index: appDelegate.sharedManager.actualOpenFileIndex, isRefreshing: isRefreshing)
        }
        
        if appDelegate.sharedManager.selectedFiles.isEmpty {
            noSelectedFiles()
        }
        DispatchQueue.main.async {
            self.pickerSelectFilesView.reloadAllComponents()
        }
    }
    
    
    
    func executeMessage(message: AnyObject, peerID: MCPeerID) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "receivedMPCShowFile"), object: MessageWithSender(message: message, sender: peerID) as AnyObject)
    }
    
    
    func indexOfFileInSelectedArray(myFile:MessageFile) -> Int {
        for (i, file) in appDelegate.sharedManager.selectedFiles.enumerated() {
            if file.getFileStdPath() == myFile.getFileStdPath() {
                return i
            }
        }
        return -1
    }
    
    func refreshMe() {
        validateOpeningFile(isRefreshing: true)
        setPresenterColor()
        self.appDelegate.mpcManager.reloadTabBarBadgetNumbers()
    }
    

    
    @objc func handleNotifications(notification: NSNotification){
        var isWaitingForFile = false
        let myMessageSender = notification.object as! MessageWithSender
        let receivedDataDictionary = myMessageSender.message as! Dictionary<String, AnyObject>
        if (receivedDataDictionary["switchAnotherFile"] != nil){
            let myFile = receivedDataDictionary["switchAnotherFile"] as! MessageFile
            if !self.existsFileInDirectory(filePath: myFile.getFileStdPath()) {
                //request sender for document file
                isWaitingForFile = true
                var messageDictionary: [String: AnyObject] = ["requestForSendingDocumentFile": myFile as AnyObject]
                self.appDelegate.mpcManager.sendDataToPeer(dictionaryWithData: messageDictionary, toPeer: myMessageSender.sender)
            }
            let isFileInArrayIndex = indexOfFileInSelectedArray(myFile: myFile)
            if(appDelegate.sharedManager.selectedFiles.count > myFile.indexInSelectedFiles){
                if(appDelegate.sharedManager.selectedFiles[myFile.indexInSelectedFiles].getFileStdPath() == myFile.getFileStdPath()){
                    appDelegate.sharedManager.actualOpenFileIndex = myFile.indexInSelectedFiles
                    openFile(index: appDelegate.sharedManager.actualOpenFileIndex, isRefreshing: false)
                } else if isFileInArrayIndex > -1 {
                    appDelegate.sharedManager.actualOpenFileIndex = isFileInArrayIndex
                    openFile(index: appDelegate.sharedManager.actualOpenFileIndex, isRefreshing: false)
                }  else {
                    appDelegate.sharedManager.selectedFiles.insert(myFile, at: myFile.indexInSelectedFiles)
                    appDelegate.sharedManager.actualOpenFileIndex = appDelegate.sharedManager.selectedFiles.count-1
                    openFile(index: appDelegate.sharedManager.actualOpenFileIndex, isRefreshing: false)
                }
            }else {
                appDelegate.sharedManager.selectedFiles.append(myFile)
                appDelegate.sharedManager.actualOpenFileIndex = appDelegate.sharedManager.selectedFiles.count-1
                openFile(index: appDelegate.sharedManager.actualOpenFileIndex, isRefreshing: false)
            }
        } else if (receivedDataDictionary["setNewZoomAndScroll"] != nil){
            if userDefaults.bool(forKey: "zoom_scroll_gestures") {
                let zoomAndScroll = receivedDataDictionary["setNewZoomAndScroll"] as! ZoomAndScroll
                self.setScrollAndZoomToWebView(sender: zoomAndScroll)
            }
        } else if ((receivedDataDictionary["sendingNewDocumentFile"] != nil)) {
            let messageFileWithData = receivedDataDictionary["sendingNewDocumentFile"] as! MessageFileWithData
            saveFileToDirectory(filePath: messageFileWithData.messageFile.getFileStdPath(), file: messageFileWithData.file)
            refreshMe()
        }
        if !isWaitingForFile{
            refreshMe()
        }  else {
            DispatchQueue.main.async {
                self.loadingWheel.isHidden = false
            }
        }
    }
    
    private func existsFileInDirectory(filePath: String) -> Bool {
        let fileManager = FileManager.default
        return fileManager.fileExists(atPath: filePath)
    }
    
    private func saveFileToDirectory(filePath: String, file: Data){
        let fileManager = FileManager.default
        let docURL = URL(fileURLWithPath:filePath)
        do {
            try file.write(to: docURL, options: .atomicWrite)
        } catch {
            print(error)
        }
    }

    
    func switchAnotherFileMessage(row: Int){
        if !appDelegate.sharedManager.selectedFiles.isEmpty {
            let selectedFile = appDelegate.sharedManager.selectedFiles[row]
            selectedFile.indexInSelectedFiles = row
            let messageDictionary: [String: AnyObject] = ["switchAnotherFile": selectedFile as AnyObject]
            self.appDelegate.mpcManager.sendDataToAllConnectedPeers(dictionaryWithData: messageDictionary)
        }
    }
    
    func deleteSelectedFileMessage(index: Int){
        if !appDelegate.sharedManager.selectedFiles.isEmpty && appDelegate.sharedManager.selectedFiles.count > index{
            let selectedFile = appDelegate.sharedManager.selectedFiles[index]
            selectedFile.indexInSelectedFiles = index
            let messageDictionary: [String: AnyObject] = ["deleteSelectedFileFromFileBrowserVC": selectedFile as AnyObject]
            self.appDelegate.mpcManager.sendDataToAllConnectedPeers(dictionaryWithData: messageDictionary)
        }
    }
    

    
    @IBAction func removeSelectedFile(_ sender: Any) {
        if appDelegate.sharedManager.selectedFiles.count > 0 {
            if appDelegate.sharedManager.selectedFiles.count > appDelegate.sharedManager.actualOpenFileIndex {
                let index = appDelegate.sharedManager.actualOpenFileIndex
                deleteSelectedFileMessage(index: index)
                appDelegate.sharedManager.selectedFiles.remove(at: index)
                setIndexOfSelectedFile(deletedAt: index)
                validateOpeningFile(isRefreshing: true)
            }
        } else {
            noSelectedFiles()
        }
        refreshMe()
    }
    
    private func setIndexOfSelectedFile(deletedAt: Int){
        if !appDelegate.sharedManager.selectedFiles.isEmpty {
            if appDelegate.sharedManager.selectedFiles.count > deletedAt {
                appDelegate.sharedManager.actualOpenFileIndex = deletedAt
            } else {
                appDelegate.sharedManager.actualOpenFileIndex = appDelegate.sharedManager.selectedFiles.count-1
            }
        } else {
            appDelegate.sharedManager.actualOpenFileIndex = -1
        }
    }
    
    func noSelectedFiles() {
        self.appDelegate.mpcManager.reloadTabBarBadgetNumbers()
        let alertController = UIAlertController(title: "Warning!", message: "Please select any file to Playlist!", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            self.showElementsUI()
            self.webView.loadRequest(URLRequest.init(url: URL.init(string: "about:blank")!))
            self.topNavigationBar.topItem?.title = "Presentation"
        })
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func forwardSwitch(_ sender: Any) {
        switchLeftFile(sender)
    }
    
    @IBAction func backwardSwitch(_ sender: Any) {
        switchRightFile(sender)
    }

    
    
    @IBAction func switchLeftFile(_ sender: Any) {
        previous.isEnabled = true
        if (appDelegate.sharedManager.actualOpenFileIndex+1 < appDelegate.sharedManager.selectedFiles.count) {
            switchAnotherFileMessage(row: appDelegate.sharedManager.actualOpenFileIndex+1)
            openFile(index: appDelegate.sharedManager.actualOpenFileIndex+1, isRefreshing: false)
        } else {
            nextSong.isEnabled = false
        }
    }
    @IBAction func switchRightFile(_ sender: Any) {
        nextSong.isEnabled = true
        if (appDelegate.sharedManager.actualOpenFileIndex-1 >= 0) {
            switchAnotherFileMessage(row: appDelegate.sharedManager.actualOpenFileIndex-1)
            openFile(index: appDelegate.sharedManager.actualOpenFileIndex-1, isRefreshing: false)
        } else {
            previous.isEnabled = false
        }
    }
    
    @IBAction func tapOnWebView(_ sender: Any) {
        if(!self.pickerSelectFilesView.isHidden){
            hideElementsUI()
        } else {
            showElementsUI()
        }
    }
    
    @IBAction func makeMeAsPresenter(_ sender: Any) {
        if !self.appDelegate.mpcManager.isDevicePresenter() {
            appDelegate.mpcManager.setMeAsPresenter()
            setPresenterColor()
        } else {
            appDelegate.mpcManager.disableMeAsPresenter()
            setPresenterColor()
        }
    }
    
    private func setPresenterColor(){
        DispatchQueue.main.async {
            if self.appDelegate.mpcManager.isDevicePresenter() {
                self.viewUnderDevicesAndSongs.backgroundColor = UIColor(red: 255/255, green: 242/255, blue: 244/255, alpha: 0.9)
            } else {
                self.viewUnderDevicesAndSongs.backgroundColor = UIColor.white
            }
        }
    }
    
    @objc private func hideElementsUI(){
        if !self.appDelegate.sharedManager.selectedFiles.isEmpty {
            self.pickerSelectFilesView.isHidden = true
            self.pickerSelectFilesView.reloadAllComponents()
            self.topNavigationBar.isHidden = true
            self.viewUnderDevicesAndSongs.isHidden = true
            self.tabBarController?.tabBar.isHidden = true
            if let topController = UIApplication.topViewController() {
                if topController == self {
                    self.webView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = false
                } else {
                    self.webView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
                    self.tabBarController?.tabBar.isHidden = false
                }
            }
        }
        setNeedsStatusBarAppearanceUpdate()
        timer?.invalidate()
    }
    
    private func showElementsUI(){
        self.pickerSelectFilesView.isHidden = false
        self.pickerSelectFilesView.reloadAllComponents()
        self.topNavigationBar.isHidden = false
        self.viewUnderDevicesAndSongs.isHidden = false
        self.tabBarController?.tabBar.isHidden = false
        self.webView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        setNeedsStatusBarAppearanceUpdate()
        timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.hideElementsUI), userInfo: nil, repeats: false)
    }

    
    override func viewWillAppear(_ animated: Bool) {
        hideElementsUI()
        validateOpeningFile(isRefreshing: true)
        previous.isEnabled = true
        nextSong.isEnabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.webView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    override var prefersStatusBarHidden: Bool {
        return self.pickerSelectFilesView.isHidden
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func openFile(index: Int, isRefreshing: Bool){
        if (!self.appDelegate.sharedManager.selectedFiles.isEmpty && self.appDelegate.sharedManager.selectedFiles.count > index && self.webView != nil){
            if !isRefreshing {
                self.loadingWheel.isHidden = false
            }
            let myFile = self.appDelegate.sharedManager.selectedFiles[index]
            self.appDelegate.sharedManager.actualOpenFileIndex = index
            var url = URL(fileURLWithPath:(myFile.getFileStdPath()))
            let urlRequest = URLRequest(url: url)
            DispatchQueue.main.async {
                if isRefreshing && self.webView.request?.url?.path == urlRequest.url?.path {
                    return
                } else {
                    //tu si z SQL zistim ci existuje nejaky message ku pesnicke a zavolam MIDI Managera
                    //tu budem aj robit statistiky otvoreni suboru
                    //MIDIManager.sendControlMessage()
                    if let song = SongRepository.findBy(item: Song(url: myFile.standardizedPath)){
                        MIDI_Manager.sendOpenSongMessage(song: song)
                        song.count_of_open = Int64(song.count_of_open+1)
                        song.today_open = true
                        SongRepository.update(item: song)
                    } else {
                        //vytvor novy zaznam v DB
                        SongRepository.insert(item: Song(url: myFile.standardizedPath, count_of_open: 1, today_open: true))
                    }
                    self.topNavigationBar.topItem?.title = "\(index+1). \((myFile.displayName))"
                    self.pickerSelectFilesView.selectRow(index, inComponent: 0, animated: true)
                    self.pickerSelectFilesView.reloadAllComponents()
                    self.webView?.loadRequest(urlRequest)
                }
                self.loadingWheel.isHidden = true
            }
        }
    }
}
