//
//  DevicesViewController.swift
//  MeshNetworks
//
//  Created by Tibor Postek on 8.8.17.
//  Copyright © 2017 Tibor Postek. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class SearchDevicesViewController: UIViewController{
    
    var peerID: MCPeerID!
    var mcAdvertiserAssistant: MCAdvertiserAssistant!
    
    @IBOutlet weak var tableOfDevices: UITableView!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate    
    
    @IBOutlet weak var navbar: UINavigationBar!
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.register(defaults: [String : Any]())
        UserDefaults.standard.synchronize()
        tableOfDevices.delegate = self
        tableOfDevices.dataSource = self
        appDelegate.mpcManager.delegate = self
        reloadTable()
        setRefreshPanel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        reloadTable()        
    }
    
    private func setRefreshPanel() {
        refreshControl.backgroundColor = UIColor.groupTableViewBackground
        if #available(iOS 10.0, *) {
            tableOfDevices.refreshControl = refreshControl
        } else {
            tableOfDevices.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshTableData(_:)), for: .valueChanged)
    }
    
    @objc private func refreshTableData(_ sender: Any) {
        OperationQueue.main.addOperation {
            self.tableOfDevices.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    
}
