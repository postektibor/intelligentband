//
//  DirectoryViewController.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 22.12.17.
//  Copyright © 2017 Tibor Postek. All rights reserved.
//

import UIKit
import SQLite

class DocumentMIDIViewController: UIViewController {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var song: Song?
    @IBOutlet weak var songID: UITextField!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var MSB: UITextField!
    @IBOutlet weak var MSBData: UITextField!
    @IBOutlet weak var LSB: UITextField!
    @IBOutlet weak var LSBData: UITextField!
    @IBOutlet weak var genresPicker: UIPickerView!

    
    var pickerData: [String] = [String]()
    var validNumbers = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navBar.topItem?.title = self.appDelegate.sharedManager.editedSongDocument?.displayName
        songID.delegate = self
        MSB.delegate = self
        MSBData.delegate = self
        LSB.delegate = self
        LSBData.delegate = self
        self.genresPicker.delegate = self
        self.genresPicker.dataSource = self
        loadGenresFromDB()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadSongFromDB(initial: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
        
    
    func loadGenresFromDB(){
        if let genres = GenreRepository.findAll() {
            for gen in genres {
                pickerData.append(gen.name!)
            }
        }
        
    }
    
    func loadSongFromDB(initial: Bool){
        if let song = SongRepository.findBy(item: Song(url: MessageFile(displayName: (self.appDelegate.sharedManager.editedSongDocument?.displayName)!, filePath: (self.appDelegate.sharedManager.editedSongDocument!.filePath)).standardizedPath)){
            self.song = song
            if initial {
                var songID = ""
                if song.msb != -1  {
                    self.MSB.text = "\(song.msb)"
                }
                if song.msb_data != -1  {
                    self.MSBData.text = "\(song.msb_data)"
                    songID += "\(song.msb_data)"
                }
                if song.lsb != -1  {
                    self.LSB.text = "\(song.lsb)"
                }
                if song.lsb_data != -1  {
                    self.LSBData.text = "\(song.lsb_data)"
                    if Int(song.lsb_data) < 10 {
                        songID += "0"
                    }
                    songID += "\(song.lsb_data)"
                }
                if !songID.isEmpty {
                    self.songID.text = "\(Int(songID)!)"
                }
                self.genresPicker.selectRow(Int(song.genre_id), inComponent: 0, animated: false)
            }
        }
    }
    func validateSongID(){
        let songIDnumber = Int(songID.text!)!
        validNumbers = true
        if songIDnumber < 0 || songIDnumber > 9999 {
            validNumbers = false
            let alert = UIAlertController(title: "Warning", message: "Possible Song ID is between 0 and 9999.", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
            alert.addAction(okAction)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()){
                UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
            }
        }
        if validNumbers {
            setSongID()
        }
    }
    
    func setSongID(){
        let songIDnumber = Int.init(songID.text!)!
        let thousand = Int(floor(Double(songIDnumber/1000)))
        let hundred = Int(floor(Double((songIDnumber - thousand*1000)/100)))
        let decimal = Int(floor(Double((songIDnumber - thousand*1000 - hundred*100)/10)))
        let unit = Int(floor(Double(songIDnumber - thousand*1000 - hundred*100 - decimal*10)))
        
        self.MSB.text = "2"
        self.MSBData.text = "\(thousand)\(hundred)"
        self.LSB.text = "64"
        self.LSBData.text = "\(decimal)\(unit)"
        storeMSB()
        storeLSB()
    }
    
    func storeMSB(){
        let newGenreID = Int64(self.genresPicker.selectedRow(inComponent: 0))
        let msbID = Int64.init(MSB.text!)!
        let msbData = Int64.init(MSBData.text!)!
        let url = MessageFile(displayName: (self.appDelegate.sharedManager.editedSongDocument?.displayName)!, filePath: (self.appDelegate.sharedManager.editedSongDocument?.filePath)!).standardizedPath
        loadSongFromDB(initial: false)
        if song == nil {
            //insert
            let newSong = Song(url: url, msb: msbID, msb_data: msbData, lsb: -1, lsb_data: -1, genre_id: newGenreID)
            SongRepository.insert(item: newSong)
        } else {
            //update
            song?.msb=msbID
            song?.msb_data = msbData
            song?.genre_id = newGenreID
            SongRepository.update(item: song!)
        }
        
        
        
    }
    
    func storeLSB(){
        let newGenreID = Int64(self.genresPicker.selectedRow(inComponent: 0))
        let lsbID = Int64.init(LSB.text!)!
        let lsbData = Int64.init(LSBData.text!)!
        let url = MessageFile(displayName: (self.appDelegate.sharedManager.editedSongDocument?.displayName)!, filePath: (self.appDelegate.sharedManager.editedSongDocument?.filePath)!).standardizedPath
        loadSongFromDB(initial: false)
        if song == nil {
            //insert
            let newSong = Song(url: url, msb: -1, msb_data: -1, lsb: lsbID, lsb_data: lsbData, genre_id: newGenreID)
            SongRepository.insert(item: newSong)
        } else {
            //update
            song?.lsb=lsbID
            song?.lsb_data = lsbData
            song?.genre_id = newGenreID
            SongRepository.update(item: song!)
        }
    }
    
    func storeGenre(){
        let newGenreID = Int64(self.genresPicker.selectedRow(inComponent: 0))
        let url = MessageFile(displayName: (self.appDelegate.sharedManager.editedSongDocument?.displayName)!, filePath: (self.appDelegate.sharedManager.editedSongDocument?.filePath)!).standardizedPath
        loadSongFromDB(initial: false)
        if song == nil {
            //insert
            let newSong = Song(url: url, msb: -1, msb_data: -1, lsb: -1, lsb_data: -1, genre_id: newGenreID)
            SongRepository.insert(item: newSong)
        } else {
            //update
            song?.genre_id = newGenreID
            SongRepository.update(item: song!)
        }
    }
    
    func storeClearValues(){
        loadSongFromDB(initial: false)
        if song != nil {
            //update
            song?.msb = -1
            song?.msb_data = -1
            song?.lsb = -1
            song?.lsb_data = -1
            song?.music_url = nil
            SongRepository.update(item: song!)
        }
    }
    
    func validateMSB(){
        let msbID = Int.init(MSB.text!)!
        let msbData = Int.init(MSBData.text!)!
        validNumbers = true
        if msbID < 0 || msbID > 99 {
            validNumbers = false
            let alert = UIAlertController(title: "Warning", message: "Possible MSB is between 0 and 99.", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
            alert.addAction(okAction)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()){
                UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
            }
        }
        if msbData < 0 || msbData > 99 {
            validNumbers = false
            let alert = UIAlertController(title: "Warning", message: "Possible MSB Data is between 0 and 99.", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
            alert.addAction(okAction)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()){
                UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
            }
        }
        if validNumbers {
            storeMSB()
        }
    }
    
    func validateLSB(){
        let lsbID = Int.init(LSB.text!)!
        let lsbData = Int.init(LSBData.text!)!
        validNumbers = true
        if lsbID < 0 || lsbID > 99 {
            validNumbers = false
            let alert = UIAlertController(title: "Warning", message: "Possible LSB is between 0 and 99.", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
            alert.addAction(okAction)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()){
                UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
            }
        }
        if lsbData < 0 || lsbData > 99 {
            validNumbers = false
            let alert = UIAlertController(title: "Warning", message: "Possible LSB Data is between 0 and 99.", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
            alert.addAction(okAction)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()){
                UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
            }
        }
        if validNumbers {
            storeLSB()
        }
    }
    
    
    @IBAction func saveSettings(_ sender: Any) {
        storeClearValues()
        if (songID.text?.isEmpty)! {
            if genresPicker.selectedRow(inComponent: 0) >= 0 {
                storeGenre()
            }
            if !(MSBData.text?.isEmpty)! && !(MSB.text?.isEmpty)! {
                validateMSB()
            }
            if !(LSBData.text?.isEmpty)! && !(LSB.text?.isEmpty)! {
                validateLSB()
            }
        } else {
            validateSongID()
        }        
        if validNumbers {
            goToFileExplorer()
        }
    }
    
    
  
    
    @IBAction func cancelSettings(_ sender: Any) {
        goToFileExplorer()
    }
    
    
    private func goToFileExplorer(){
        self.appDelegate.sharedManager.editedSongDocument = nil
        OperationQueue.main.addOperation { () -> Void in
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
