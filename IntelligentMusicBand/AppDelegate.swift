//
//  AppDelegate.swift
//  MeshNetworks
//
//  Created by Tibor Postek on 8.8.17.
//  Copyright © 2017 Tibor Postek. All rights reserved.
//

import UIKit
import CoreData
import SwiftyDropbox
import MultipeerConnectivity
import SQLite
import Fabric
import Crashlytics



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var mpcManager: MPCManger!
    var sharedManager: FilesManager!
    let userDefaults = UserDefaults.standard
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.  
        enableMPConnectivity()
        initMIDICannel()
        initDatabaseConnection()
        
        sharedManager = FilesManager.shared
        DropboxClientsManager.setupWithAppKey("4yaafzytcegr6eo")
        Fabric.with([Crashlytics.self])
        return true
    }
    
    private func enableMPConnectivity(){
        mpcManager=MPCManger()
        mpcManager.browser.startBrowsingForPeers()
        if UserDefaults.standard.bool(forKey: "visibility_switch"){
            mpcManager.advertiser.startAdvertisingPeer()
        }       
        mpcManager.delegate?.refreshing()
    }
    
    private func initMIDICannel() {
        let midiChannel: Int = self.userDefaults.integer(forKey: "midi_control_channel")
        if midiChannel == 0 {
            self.userDefaults.set(16, forKey: "midi_control_channel")
        }
    }

    private func initDatabaseConnection(){
        //path to SQLite database file
        let path = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent("SongsMIDIMessages.sqlite").path
        //Database connection
        let db = try! Connection(path)
        
        //initializing repositories
        GenreRepository.repositoryInit(db: db)
        SongRepository.repositoryInit(db: db)
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if let authResult = DropboxClientsManager.handleRedirectURL(url) {
            switch authResult {
            case .success:
                print("Success! User is logged into Dropbox.")
            case .cancel:
                print("Authorization flow was manually canceled by user!")
            case .error(_, let description):
                print("Error: \(description)")
            }
        }
        return true
    }
    
    func getViewController(identifierVC: String) -> UIViewController {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: identifierVC)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
      //  mpcManager.storeAllConnectedDevice()
        mpcManager.disconnectMeFromSession()
        mpcManager.clearAllFoundPeers()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        mpcManager.delegate?.refreshing()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        //self.saveContext()
    }

}

