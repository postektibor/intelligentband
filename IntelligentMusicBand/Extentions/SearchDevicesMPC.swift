//
//  DevicesMPC.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 2.1.18.
//  Copyright © 2018 Tibor Postek. All rights reserved.
//

import Foundation
import MultipeerConnectivity

extension SearchDevicesViewController: I_MPCManager {
    
    func foundPeer() {
        reloadTable()
    }
    
    
    func lostPeer() {
        reloadTable()
    }
    
    
    func connectedWithPeer(peerID: MCPeerID) {
        reloadTable()
    }
    
    func reloadTable(){
        DispatchQueue.main.async {
            self.tableOfDevices.reloadData()
            if self.appDelegate.mpcManager.coutOfUnconnectedDevices() > 0 {
                self.tabBarItem.badgeValue = "\(self.appDelegate.mpcManager.coutOfUnconnectedDevices())"
            } else {
                self.tabBarItem.badgeValue = nil
            }
        }
    }
    
    func refreshing() {
        appDelegate.mpcManager.delegate = self
        reloadTable()
    }
    
    
    func disconnectedFromPeer(peerID: MCPeerID, session: MCSession) {
        reloadTable()
    }
}
