//
//  SelectedFilesTable.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 3.1.18.
//  Copyright © 2018 Tibor Postek. All rights reserved.
//

import Foundation
import UIKit

extension PlaylistViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appDelegate.sharedManager.selectedFiles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "seletedFilesCell") as UITableViewCell!
        if appDelegate.sharedManager.selectedFiles.count > indexPath.row {
            let selectedFile = appDelegate.sharedManager.selectedFiles[indexPath.row]
            cell.textLabel?.text = "\(indexPath.row+1). \(selectedFile.displayName)"
        }
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor(red: 255/255, green: 45/255, blue: 85/255, alpha: 0.9)
        cell.selectedBackgroundView = bgColorView
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath){
        if editingStyle == UITableViewCellEditingStyle.delete {
            let selectedFile = appDelegate.sharedManager.selectedFiles[indexPath.row]
            selectedFile.indexInSelectedFiles = indexPath.row            
            let messageDictionary2: [String: AnyObject] = ["deleteSelectedFileFromFileBrowserVC": selectedFile as AnyObject]
            self.appDelegate.mpcManager.sendDataToAllConnectedPeers(dictionaryWithData: messageDictionary2)
            appDelegate.sharedManager.selectedFiles.remove(at: indexPath.row)
            FilesManager.shared.refreshDelegates()
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            self.reloadTable()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {      
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
