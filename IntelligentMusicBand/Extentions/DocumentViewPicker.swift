//
//  ShowFilePickerView.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 3.1.18.
//  Copyright © 2018 Tibor Postek. All rights reserved.
//

import Foundation
import UIKit

extension DocumentView_ViewController: UIPickerViewDataSource, UIPickerViewDelegate {
   
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return appDelegate.sharedManager.selectedFiles.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if appDelegate.sharedManager.selectedFiles.count > row {
            return "\(row+1). \((appDelegate.sharedManager.selectedFiles[row].displayName))"
        }
        return"\(row+1). Failed file number!"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switchAnotherFileMessage(row: row)
        self.openFile(index: row, isRefreshing: false)
    }
}
