import Foundation
import UIKit

extension DocumentBrowserViewController: UITableViewDataSource, UITableViewDelegate, FileCellDelegate {
        
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return filteredFiles.count
        }
        return files.count
    }
    
    private func _configureCell(cell: FileBrowserTableCell, atIndexPath indexPath: IndexPath) {
        cell.queue.cancelAllOperations()
        
        let operation: BlockOperation = BlockOperation()
        operation.addExecutionBlock { [weak operation] () -> Void in
            var color = UIColor.black
            let selectedFile = self.fileForIndexPath(indexPath)
            let rating = self.parser.getRating(file: selectedFile)
            let documentFile = MessageFile.toMyFile(fbFile: selectedFile)
            var hasMusicFile = false
            cell.cellDelegate = self
            if !selectedFile.isDirectory && SongRepository.findBy(item: Song(url: documentFile.standardizedPath)) != nil{
                let song = SongRepository.findBy(item: Song(url: documentFile.standardizedPath))
                if (song?.today_open)! {
                    color = UIColor(red:0.10, green:0.03, blue:0.62, alpha:0.7)
                }
                if song?.music_url != nil {
                    hasMusicFile = true
                }
            }
            
            DispatchQueue.main.async { [weak operation] () -> Void in
                if let operation = operation, operation.isCancelled { return }
                cell.selectionStyle = .blue
                cell.name.textColor = color
                cell.starRating.rating = rating
                self.setCellDetailLogo(selectedFile :selectedFile, cell: cell)
                cell.name.text = selectedFile.displayName
                cell.logo.image = selectedFile.type.image()                
            }
        }        
        cell.queue.addOperation(operation)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? FileBrowserTableCell
        {
            _configureCell(cell: cell, atIndexPath: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "FileCell", for: indexPath) as? FileBrowserTableCell
        if cell == nil
        {
            cell = FileBrowserTableCell(style:.default, reuseIdentifier: "FileCell")
        }
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor(red: 255/255, green: 45/255, blue: 85/255, alpha: 0.9)
        cell?.selectedBackgroundView = bgColorView
        return cell!
    }

    func setCellDetailLogo(selectedFile : BrowserFile, cell:FileBrowserTableCell){
        if selectedFile.isDirectory {
            if cell.detailLogo != nil {
                cell.detailLogo.isHidden = true
            }
            if cell.starRating != nil {
                cell.starRating.isHidden = true
            }
            cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        } else {
            if cell.detailLogo != nil {
                cell.detailLogo.isHidden = false
            }
            if cell.starRating != nil {
                cell.starRating.isHidden = false
            }
            cell.accessoryType = .none
        }
    }
    
    func getCurrentCellIndexPath(_ sender: UIButton) -> IndexPath? {
        let buttonPosition = sender.convert(CGPoint.zero, to: tableView)
        if let indexPath: IndexPath = tableView.indexPathForRow(at: buttonPosition) {
            return indexPath
        }
        return nil
    }
    
    func didPressMIDIDetailButton(_ sender: UIButton) {
        if let indexPath = getCurrentCellIndexPath(sender) {
            self.appDelegate.sharedManager.editedSongDocument = fileForIndexPath(indexPath)
            if !(self.appDelegate.sharedManager.editedSongDocument?.isDirectory)! {
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "showFileMIDIDetail", sender: nil)
                }
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedFile = fileForIndexPath(indexPath)
        if selectedFile.isDirectory {
            self.loadingWheel.isHidden = false
            DispatchQueue.global().async {
                self.appDelegate.sharedManager.arrayOfOpenPaths.append(selectedFile.filePath)
                self.files = self.parser.filesForDirectory(selectedFile.filePath, selectedSortingSegmentIndex: self.sortingSegment.selectedSegmentIndex)               
                DispatchQueue.main.async {
                    self.topNavigationBar.topItem?.title = selectedFile.displayName
                    self.indexFiles()
                    self.tableView.reloadData()
                    self.loadingWheel.isHidden = true
                    
                }
            }
        }
        else {
            var messageDictionary: [String: AnyObject] = ["addNewSelectedFileFileBrowserVC": MessageFile.toMyFile(fbFile: selectedFile) as AnyObject]
            self.appDelegate.mpcManager.sendDataToAllConnectedPeers(dictionaryWithData: messageDictionary)     
            
            var wasFileSelected = false
            for selFile in appDelegate.sharedManager.selectedFiles {
                if selFile.getFileStdPath() == MessageFile.toMyFile(fbFile: selectedFile).getFileStdPath() {
                    wasFileSelected = true
                }
            }
            if !wasFileSelected {
                appDelegate.sharedManager.selectedFiles.append(MessageFile.toMyFile(fbFile: selectedFile))
                FilesManager.shared.refreshDelegates()
            }
            let childView = self.childViewControllers.last as! PlaylistViewController
            childView.reloadTable()
            tableView.deselectRow(at: indexPath, animated: true)
        }
        if self.isSearching {
            self.isSearching = false
            self.searchController.isActive = false
            self.tableView.reloadData()
        }
    }
}
