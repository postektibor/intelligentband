//
//  SetTabBarBadget.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 6.1.18.
//  Copyright © 2018 Tibor Postek. All rights reserved.
//

import Foundation
import UIKit

extension UITabBarController {
    func updateBadge(indexOfTab: Int, num: Int) {        
        DispatchQueue.main.async {
            let tabItem = self.tabBar.items![indexOfTab]
            if num > 0 {
                tabItem.badgeValue = "\(num)"
            } else {
                tabItem.badgeValue = nil
            }
        }
    }
}
