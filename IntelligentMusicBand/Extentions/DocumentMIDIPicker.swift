//
//  Picker.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 3.1.18.
//  Copyright © 2018 Tibor Postek. All rights reserved.
//

import Foundation
import UIKit

extension DocumentMIDIViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
}
