//
//  DevicesUITableView.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 2.1.18.
//  Copyright © 2018 Tibor Postek. All rights reserved.
//

import UIKit
import MultipeerConnectivity

extension SearchDevicesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {        
        return appDelegate.mpcManager.getAllDevices().count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "devicesCell", for: indexPath) as! DevicesTableViewCell
        if appDelegate.mpcManager.getAllDevices().count > indexPath.row {
            let device = appDelegate.mpcManager.getAllDevices()[indexPath.row]
            
            if appDelegate.mpcManager.isDeviceConnected(peer: device){
                cell.name?.text = device.displayName
                cell.logo.tintColor = UIColor(red: 255/255, green: 45/255, blue: 85/255, alpha: 0.9)
                navbar.topItem?.title = "Founded devices"
            } else {
                cell.name?.text = device.displayName
                cell.logo.tintColor = UIColor(red:0.45, green:0.45, blue:0.45, alpha:1.0)
                navbar.topItem?.title = "Founded devices"
            }
        }
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor(red: 255/255, green: 45/255, blue: 85/255, alpha: 0.9)
        cell.selectedBackgroundView = bgColorView
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if appDelegate.mpcManager.getAllDevices().count > indexPath.row {
            let device = appDelegate.mpcManager.getAllDevices()[indexPath.row]
            DispatchQueue.main.async {
                if self.appDelegate.mpcManager.isDeviceConnected(peer: device){
                    self.appDelegate.mpcManager.disconnectDevice(device: device)
                } else {
                    self.appDelegate.mpcManager.browser.invitePeer(device, to: self.appDelegate.mpcManager.session, withContext: nil, timeout: 20)
                    self.appDelegate.mpcManager.storeMyInvitedDevices(device: device)
                }
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
