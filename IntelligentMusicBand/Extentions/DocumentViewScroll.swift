//
//  Scroll.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 3.1.18.
//  Copyright © 2018 Tibor Postek. All rights reserved.
//

import Foundation
import UIKit

extension DocumentView_ViewController: UIScrollViewDelegate {
    
    func setScrollAndZoomToWebView(sender: ZoomAndScroll){
        DispatchQueue.main.async {
            if self.webView != nil {
                self.webView.scrollView.contentSize = CGSize(width: sender.width, height: sender.height)
                self.webView.scrollView.setZoomScale(CGFloat(sender.scale_Zoom), animated: false)
                self.webView.scrollView.setContentOffset(CGPoint(x: sender.x_Scroll, y: sender.y_Scroll), animated: false)
            }
        }
    }
    
    func sendZoomAndScrollMessage(scrollView: UIScrollView){
        let zoomAndScroll = ZoomAndScroll(x_Scroll: Double(scrollView.contentOffset.x), y_Scroll: Double(scrollView.contentOffset.y), scale_Zoom: Double(scrollView.zoomScale), width: Double(scrollView.bounds.width), height: Double(scrollView.bounds.height))
        if appDelegate.mpcManager.isDevicePresenter() {
            let messageDictionary: [String: AnyObject] = ["setNewZoomAndScroll": zoomAndScroll as AnyObject]
            self.appDelegate.mpcManager.sendDataToAllConnectedPeers(dictionaryWithData: messageDictionary)
        }
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            sendZoomAndScrollMessage(scrollView: scrollView)
            isScrolling = false
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        sendZoomAndScrollMessage(scrollView: scrollView)
        isScrolling = false
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        isScrolling = true
    }
    func scrollViewDidZoom(_ scrollView: UIScrollView){
        if !isScrolling {
            sendZoomAndScrollMessage(scrollView: scrollView)
        }
    }
}
