import Foundation
import UIKit

public class BrowserFile: NSObject {

    let displayName: String
    let isDirectory: Bool
    let fileExtension: String?
    let fileAttributes: NSDictionary?
    let filePath: URL
    let type: FBFileType
    
    var rating: Double
    
    open func delete()
    {
        do
        {
            try FileManager.default.removeItem(at: self.filePath)
        }
        catch
        {
            print("An error occured when trying to delete file:\(self.filePath) Error:\(error)")
        }
    }
    
    func isDirectoryCompare(file2:BrowserFile) -> Int {
        if self.isDirectory {
            if file2.isDirectory {
                if self.displayName.lowercased() > file2.displayName.lowercased(){
                    return -1
                } else if self.displayName.lowercased() < file2.displayName.lowercased(){
                    return 1
                } else {
                    return 2
                }
            }else {
                return 2
            }
        }else {
            if file2.isDirectory {
                return -2
            }else {
                if self.displayName.lowercased() > file2.displayName.lowercased(){
                    return -1
                } else if self.displayName.lowercased() < file2.displayName.lowercased(){
                    return 1
                } else {
                    return 0
                }
            }
        }
    }

    
    init(filePath: URL) {
        self.rating = 0
        self.filePath = filePath
        let isDirectory = checkDirectory(filePath)
        self.isDirectory = isDirectory
        if self.isDirectory {
            self.fileAttributes = nil
            self.fileExtension = nil
            self.type = .Directory
        }
        else {
            self.fileAttributes = getFileAttributes(self.filePath)
            self.fileExtension = filePath.pathExtension
            if let fileExtension = fileExtension {
                self.type = FBFileType(rawValue: fileExtension) ?? .Default
            }
            else {
                self.type = .Default
            }
        }
        self.displayName = filePath.lastPathComponent 
    }
}

/**
 FBFile type
 */
public enum FBFileType: String {
    /// Directory
    case Directory = "directory"
    /// GIF file
    case GIF = "gif"
    /// JPG file
    case JPG = "jpg"
    /// PLIST file
    case JSON = "json"
    /// PDF file
    case PDF = "pdf"
    /// PLIST file
    case PLIST = "plist"
    /// PNG file
    case PNG = "png"
    /// ZIP file
    case ZIP = "zip"
    case MP3 = "mp3"
    case WAV = "wav"
    case AAC = "aac"
    case AIFF = "aiff"
    /// Any file
    case Default = "file"
    
    /**
     Get representative image for file type
     
     - returns: UIImage for file type
     */
    public func image() -> UIImage? {
        let bundle =  Bundle(for: FileParser.self)
        var fileName = String()
        switch self {
        case .Directory: fileName = "folder"
        case .JPG, .PNG, .GIF: fileName = "image"
        case .PDF: fileName = "pdf"
        case .ZIP: fileName = "zip"
        case .MP3: fileName = "AudioFile"
        case .AAC: fileName = "AudioFile"
        case .WAV: fileName = "AudioFile"
        case .AIFF: fileName = "AudioFile"
        default: fileName = "file"
        }
        let file = UIImage(named: fileName, in: bundle, compatibleWith: nil)
        return file
    }
}

/**
 Check if file path NSURL is directory or file.
 
 - parameter filePath: NSURL file path.
 
 - returns: isDirectory Bool.
 */
func checkDirectory(_ filePath: URL) -> Bool {
    var isDirectory = false
    do {
        var resourceValue: AnyObject?
        try (filePath as NSURL).getResourceValue(&resourceValue, forKey: URLResourceKey.isDirectoryKey)
        if let number = resourceValue as? NSNumber , number == true {
            isDirectory = true
        }
    }
    catch { }
    return isDirectory
}

func getFileAttributes(_ filePath: URL) -> NSDictionary? {
    let path = filePath.path
    let fileManager = FileParser.sharedInstance.fileManager
    do {
        let attributes = try fileManager.attributesOfItem(atPath: path) as NSDictionary
        return attributes
    } catch {}
    return nil
}
