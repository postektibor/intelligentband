//
//  MessageFileWithData.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 24.3.18.
//  Copyright © 2018 Tibor Postek. All rights reserved.
//

import Foundation
//
//  MyFile.swift
//  MeshNetworks
//
//  Created by Tibor Postek on 7.9.17.
//  Copyright © 2017 Tibor Postek. All rights reserved.
//

import Foundation

class MessageFileWithData: NSObject, NSCoding  {
    var file: Data
    var messageFile: MessageFile
    
    
    init(file: Data, messageFile: MessageFile) {
        self.file = file
        self.messageFile = messageFile
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(file, forKey: "file")
        aCoder.encode(messageFile, forKey: "messageFile")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        guard
            let file = aDecoder.decodeObject(forKey: "file") as? Data,
            let messageFile = aDecoder.decodeObject(forKey: "messageFile") as? MessageFile
            else {
                return nil
        }
        self.init(file: file, messageFile: messageFile)
        self.file = file
        self.messageFile = messageFile
    }
}
