//
//  ZoomAndScroll.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 18.11.17.
//  Copyright © 2017 Tibor Postek. All rights reserved.
//

import UIKit

class ZoomAndScroll: NSObject, NSCoding {
    var x_Scroll: Double
    var y_Scroll: Double
    var scale_Zoom: Double
    var width: Double
    var height: Double
    
    init(x_Scroll: Double, y_Scroll: Double, scale_Zoom: Double, width: Double, height: Double) {
        self.x_Scroll = x_Scroll
        self.y_Scroll = y_Scroll
        self.scale_Zoom = scale_Zoom
        self.width = width
        self.height = height
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(x_Scroll, forKey: "x_Scroll")
        aCoder.encode(y_Scroll, forKey: "y_Scroll")
        aCoder.encode(scale_Zoom, forKey: "scale_Zoom")
        aCoder.encode(width, forKey: "width")
        aCoder.encode(height, forKey: "height")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        guard
            let x_Scroll = aDecoder.decodeDouble(forKey: "x_Scroll") as? Double,
            let y_Scroll = aDecoder.decodeDouble(forKey: "y_Scroll") as? Double,
            let scale_Zoom = aDecoder.decodeDouble(forKey: "scale_Zoom") as? Double,
            let width = aDecoder.decodeDouble(forKey: "width") as? Double,
            let height = aDecoder.decodeDouble(forKey: "height") as? Double
            else{
                return nil
        }
        self.init(x_Scroll: x_Scroll, y_Scroll: y_Scroll, scale_Zoom: scale_Zoom, width:width, height:height)
    }
}
