//
//  MessageWithSender.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 24.3.18.
//  Copyright © 2018 Tibor Postek. All rights reserved.
//

import Foundation
import MultipeerConnectivity

class MessageWithSender{
    var message: AnyObject
    var sender: MCPeerID
    
    init(message: AnyObject, sender: MCPeerID) {
        self.message = message
        self.sender = sender
    }
    
}
