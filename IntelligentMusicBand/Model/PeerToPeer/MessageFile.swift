//
//  MyFile.swift
//  MeshNetworks
//
//  Created by Tibor Postek on 7.9.17.
//  Copyright © 2017 Tibor Postek. All rights reserved.
//

import Foundation

class MessageFile: NSObject, NSCoding  {
    var displayName: String
    var filePath: URL
    var standardizedPath: String
    var indexInSelectedFiles: Int
    
    
    init(displayName: String, filePath: URL) {
        self.displayName = displayName;
        self.filePath = filePath
        let documentPath = FileParser.sharedInstance.documentsURL().standardizedFileURL.relativePath
        self.standardizedPath = filePath.standardizedFileURL.relativePath.replacingOccurrences(of: documentPath, with: "")
        self.indexInSelectedFiles = -1
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(displayName, forKey: "displayName")
        aCoder.encode(filePath, forKey: "filePath")
        aCoder.encode(standardizedPath, forKey: "standardizedPath")
        aCoder.encode(indexInSelectedFiles, forKey: "indexInSelectedFiles")
    }
    
    func getFileStdPath() -> String {
       // let url = "/private\(FileParser.sharedInstance.documentsURL().relativePath)\(self.standardizedPath)"
       // return url
        return MessageFile.getFileStdPath(standardizedPath: self.standardizedPath)
    }
    
    static func getFileStdPath(standardizedPath: String) -> String {
        let url = "/private\(FileParser.sharedInstance.documentsURL().relativePath)\(standardizedPath)"
        return url
    }

    
    required convenience init?(coder aDecoder: NSCoder) {
        guard
            let displayName = aDecoder.decodeObject(forKey: "displayName") as? String,
            let stdPath = aDecoder.decodeObject(forKey: "standardizedPath") as? String,
            let index = aDecoder.decodeInteger(forKey: "indexInSelectedFiles") as? Int,
            let filePath = aDecoder.decodeObject(forKey: "filePath") as? URL
            else {
                return nil
        }
        self.init(displayName: displayName, filePath: filePath)
        self.standardizedPath = stdPath
        self.indexInSelectedFiles = index
    }
}
extension MessageFile {
    class func toMyFile(fbFile: BrowserFile?) -> MessageFile {
        return MessageFile(displayName: (fbFile?.displayName)!, filePath: (fbFile?.filePath)!)
    }
}
