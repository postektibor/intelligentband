//
//  Song.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 25.12.17.
//  Copyright © 2017 Tibor Postek. All rights reserved.
//

import Foundation

class Song {
    let url: String?
    var music_url: String?
    var msb: Int64
    var msb_data: Int64
    var lsb: Int64
    var lsb_data: Int64
    var genre_id: Int64
    var count_of_open: Int64
    var today_open: Bool
    
    init(url: String) {
        self.url = url
        self.music_url = nil
        msb = -1
        msb_data = -1
        lsb = -1
        lsb_data = -1
        genre_id = 0
        count_of_open = 0
        self.today_open = false
    }
    
    init(url: String, today_open: Bool) {
        self.url = url
        self.music_url = nil
        msb = -1
        msb_data = -1
        lsb = -1
        lsb_data = -1
        genre_id = 0
        count_of_open = 0
        self.today_open = today_open
    }
    
    init(url: String, music_url: String) {
        self.url = url
        self.music_url = music_url
        msb = -1
        msb_data = -1
        lsb = -1
        lsb_data = -1
        genre_id = 0
        count_of_open = 0
        self.today_open = false
    }
    
    init(url: String, count_of_open: Int64) {
        self.url = url
        self.music_url = nil
        msb = -1
        msb_data = -1
        lsb = -1
        lsb_data = -1
        genre_id = 0
        self.count_of_open = count_of_open
        self.today_open = false
    }
    
    init(url: String, count_of_open: Int64, today_open: Bool) {
        self.url = url
        self.music_url = nil
        msb = -1
        msb_data = -1
        lsb = -1
        lsb_data = -1
        genre_id = 0
        self.count_of_open = count_of_open
        self.today_open = today_open
    }
    
    init(url: String, msb: Int64, msb_data: Int64, lsb: Int64, lsb_data: Int64, genre_id: Int64) {
        self.url = url
        self.music_url = nil
        self.msb = msb
        self.msb_data = msb_data
        self.lsb = lsb
        self.lsb_data = lsb_data
        self.genre_id = genre_id
        self.count_of_open = 0
        self.today_open = false
    }
    init(url: String, msb: Int64, msb_data: Int64, lsb: Int64, lsb_data: Int64, genre_id: Int64, count_of_open: Int64) {
        self.url = url
        self.music_url = nil
        self.msb = msb
        self.msb_data = msb_data
        self.lsb = lsb
        self.lsb_data = lsb_data
        self.genre_id = genre_id
        self.count_of_open = count_of_open
        self.today_open = false
    }
    init(url: String, msb: Int64, msb_data: Int64, lsb: Int64, lsb_data: Int64, genre_id: Int64, count_of_open: Int64, today_open: Bool) {
        self.url = url
        self.music_url = nil
        self.msb = msb
        self.msb_data = msb_data
        self.lsb = lsb
        self.lsb_data = lsb_data
        self.genre_id = genre_id
        self.count_of_open = count_of_open
        self.today_open = today_open
    }
    
    init(url: String, music_url: String?, msb: Int64, msb_data: Int64, lsb: Int64, lsb_data: Int64, genre_id: Int64, count_of_open: Int64, today_open: Bool) {
        self.url = url
        self.music_url = music_url
        self.msb = msb
        self.msb_data = msb_data
        self.lsb = lsb
        self.lsb_data = lsb_data
        self.genre_id = genre_id
        self.count_of_open = count_of_open
        self.today_open = today_open
    }
    
}
