//
//  Genre.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 25.12.17.
//  Copyright © 2017 Tibor Postek. All rights reserved.
//

import Foundation

class Genre {    
    let genre_id: Int64
    var name: String?
    
    init(genre_id: Int64) {
        self.genre_id = genre_id
    }
    
    init(genre_id: Int64, name: String) {
        self.genre_id = genre_id
        self.name = name
    }
}


