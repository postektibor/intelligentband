//
//  OnlineDevicesTableViewCell.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 30.12.17.
//  Copyright © 2017 Tibor Postek. All rights reserved.
//

import UIKit

class OnlineDevicesTableViewCell: UITableViewCell {

    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
