//
//  FileBrowserTableCell.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 26.12.17.
//  Copyright © 2017 Tibor Postek. All rights reserved.
//

import UIKit
import Cosmos

protocol FileCellDelegate : class {
    func didPressMIDIDetailButton(_ sender: UIButton)
}

class FileBrowserTableCell: UITableViewCell {
    weak var cellDelegate: FileCellDelegate?
    
    @IBOutlet weak var starRating: CosmosView!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var detailLogo: UIButton!
    @IBOutlet weak var songNoteSelectionMP3: UIImageView!
    
    @IBAction func fileMIDIDetail(_ sender: UIButton) {
        cellDelegate?.didPressMIDIDetailButton(sender)
    }
    
    
    let queue = SerialOperationQueue()
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse()
    {
        super.prepareForReuse()
        name?.text = " "
        starRating.rating = 0
        logo.image = UIImage(named: "file", in: Bundle(for: FileParser.self), compatibleWith: nil)
        detailLogo.isHidden = true
    }

}
