//
//  SongBrowserTableViewCell.swift
//  IntelligentMusicBand
//
//  Created by Tibor Postek on 16.1.18.
//  Copyright © 2018 Tibor Postek. All rights reserved.
//

import UIKit

class SongBrowserTableViewCell: UITableViewCell {

    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    let queue = SerialOperationQueue()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
        
    override func prepareForReuse()
    {
        super.prepareForReuse()
        name?.text = " "
        logo.image = UIImage(named: "file", in: Bundle(for: FileParser.self), compatibleWith: nil)
    }

}
